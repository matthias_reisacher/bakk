﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;

public class CellPathway : EditorWindow
{
	private NavigateCamera _camera;
	private static MoleculeSpeciesListView listViewMolecules = new MoleculeSpeciesListView();
	private static ReactionTypeListView listViewReactions = new ReactionTypeListView();

    [MenuItem("Window/CellPathway")]
	public static void Init()
	{
		EditorWindow.GetWindow<CellPathway>();
	}

	void OnEnable()
	{
		hideFlags = HideFlags.HideAndDontSave;
    }

	void OnGUI()
	{
		if (_camera == null)
		{
			_camera = GameObject.FindWithTag("MainCamera").GetComponent<NavigateCamera>();
		}

		// Just for testing - can be deleted.
		/*var selectedId = SceneManager.Instance.SelectedElement;
		EditorGUILayout.LabelField("Selected Protein ID", selectedId.ToString());*/

		GUILayout.Label("Molecule Species", EditorStyles.boldLabel);

		listViewMolecules.Gui(CUE.Instance.Species);

		Space();

		GUILayout.Label("Add Molecule", EditorStyles.miniLabel);

		GUILayout.BeginHorizontal();

		if (GUILayout.Button("From PDB File"))
		{
			PdbMoleculeImporter.UserSelectFile();
		}

		if (GUILayout.Button("Download From PDB"))
		{
			PdbImportWindow.UserDownload();
		}
		GUILayout.EndHorizontal();

		Space();

		GUILayout.Label("Reactions", EditorStyles.boldLabel);

		listViewReactions.Gui(CUE.Instance.ReactionTypes);

		Space();
		GUILayout.Label("Add Reaction", EditorStyles.miniLabel);
		if (GUILayout.Button("Add Reaction"))
		{
			ReactionType item = new ReactionType();
			CUE.Instance.AddReaction(item);
			listViewReactions.FoldOpen(item);
		}

		Space();

		GUILayout.Label("Placing", EditorStyles.boldLabel);

		GUILayout.BeginHorizontal();

		GUILayout.EndHorizontal();

        var moleculesArePlaced = SceneManager.Instance.MoleculeInstanceInfos.Count > 0;

        GUI.enabled = !moleculesArePlaced;
        if (GUILayout.Button("Place Molecules"))
		{
			// Set location to selected element's position
			Dispenser.Instance.Location = SceneManager.Instance.GetSelectedElementPosition();

			foreach (var species in CUE.Instance.Species)
			{
				Dispenser.Instance.AddMolecules(species, species.InitialQuantity);
			}

			CUE.Instance.SetMolecules();
            moleculesArePlaced = true;
            SceneManager.Instance.DoCrossSectionTest();
        }

        GUI.enabled = moleculesArePlaced;
        if (GUILayout.Button("Remove all Molecules"))
		{
			CUE.Instance.RemoveMolecules();
            moleculesArePlaced = false;
        }

        GUI.enabled = true;
        Space();

        GUILayout.Label("Visualization", EditorStyles.boldLabel);

        CUE.Instance.ConeAngle = EditorGUILayout.IntSlider("Cone Angle [°]:", CUE.Instance.ConeAngle, 1, 89);
        SimulationManager.FixedVisLineColor = EditorGUILayout.ColorField("Fixed-Color", SimulationManager.FixedVisLineColor);

        Space();

		GUILayout.Label("Simulation", EditorStyles.boldLabel);

        SimulationManager.EnableFastCollisionDetection = GUILayout.Toggle(SimulationManager.EnableFastCollisionDetection, " Fast Collision Detection");
        SimulationManager.EnableCopasiOutput = GUILayout.Toggle(SimulationManager.EnableCopasiOutput, " Show Copasi Output");
        SimulationManager.CompartmentCheckInterval = EditorGUILayout.Slider("Compartment Check [s]:", SimulationManager.CompartmentCheckInterval, 0.5f, 5.0f);
        CUE.Instance.Radius = EditorGUILayout.IntField("Compartment-Radius:", CUE.Instance.Radius);
        SimulationManager.Speed = Mathf.Max(0, EditorGUILayout.FloatField("Speed:", SimulationManager.Speed));
        CUE.Instance.Volume = Mathf.Max(0, EditorGUILayout.FloatField("Volume [nl]:", (float)CUE.Instance.Volume));
        //EditorGUILayout.LabelField("Radius [nm]", Utils.GetSphereRadius(cue.Volume).ToString());
        CUE.Instance.SimulationStep = Mathf.Max(0, EditorGUILayout.FloatField("Simulation Step [s]:", (float)CUE.Instance.SimulationStep));
		CUE.Instance.VisualizationStep = Mathf.Max(0, EditorGUILayout.FloatField("Visualization Step [s]:", (float)CUE.Instance.VisualizationStep));

		Space();

        bool isSumulating = SimulationManager.IsSimulating;
        GUI.enabled = !isSumulating;
		if (GUILayout.Button("Start Simulation"))
		{
            SimulationManager.Instance.EnableSimulation();
        }
        GUI.enabled = isSumulating;
        if (GUILayout.Button("Stop Simulation"))
		{
			SimulationManager.Instance.DisableSimulation();
		}
    }

	private static void Space()
	{
		GUILayout.Space(10);
	}
}