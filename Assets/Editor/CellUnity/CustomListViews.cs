﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

public class MoleculeSpeciesListView : ListView<MoleculeSpecies>
{
    protected override void OnItemGui(MoleculeSpecies item)
    {
        if (item == null)
        {
            base.OnItemGui(item);
            return;
        }

        CUE cue = CUE.Instance;

        item.Name = EditorGUILayout.TextField("Name", item.Name);
        item.InitialQuantity = EditorGUILayout.IntField("Initial Quantity [1]", item.InitialQuantity);
        item.Mass = EditorGUILayout.FloatField("Mass [u]", item.Mass);
        EditorGUILayout.LabelField("~Size [nm]", item.Size.ToString());

        EditorUtility.SetDirty(item);

        if (GUILayout.Button("remove"))
        {
            cue.RemoveSpecies(item);
        }
    }
}


public class ReactionTypeListView : ListView<ReactionType>
{
    protected override void OnItemGui(ReactionType item)
    {
        CUE cue = CUE.Instance;

        if (item == null)
        {
            base.OnItemGui(item);
            return;
        }

        item.Name = EditorGUILayout.TextField("Name", item.Name);

        EditorGUILayout.BeginHorizontal();

        item.Reagents = GuiSpeciesList(cue, item.Reagents);

        EditorGUILayout.LabelField(" \u2192 ", GUILayout.MaxWidth(30));

        item.Products = GuiSpeciesList(cue, item.Products);

        EditorGUILayout.EndHorizontal();

        item.Rate = EditorGUILayout.FloatField("Rate [nl/(nmol*s)]", item.Rate);

        item.NeedsProtein = GUILayout.Toggle(item.NeedsProtein, " React on Protein");

        EditorUtility.SetDirty(item);

        /*if (Application.isPlaying)
        {
            if (GUILayout.Button("start reaction"))
            {
                cue.ReactionManager.InitiateReaction(item, true);
            }
        }*/

        if (GUILayout.Button("remove"))
        {
            cue.RemoveReaction(item);
        }
    }

    private MoleculeSpecies[] GuiSpeciesList(CUE cue, MoleculeSpecies[] speciesArray)
    {
        MoleculeSpeciesPopup speciesPopup = new MoleculeSpeciesPopup(cue, speciesArray.Length > 0);

        List<MoleculeSpecies> species = new List<MoleculeSpecies>(speciesArray);
        species.Add(null);

        for (int i = 0; i < species.Count; i++)
        {
            if (i != 0) { EditorGUILayout.LabelField("+", GUILayout.MaxWidth(10)); }

            species[i] = speciesPopup.Popup(species[i], (species[i] == null && i > 0));
        }

        while (species.Contains(null))
        {
            species.Remove(null);
        }

        return species.ToArray();
    }
}


public class MoleculeSpeciesPopup
{
    public MoleculeSpeciesPopup(CUE cue, bool nullPossible)
    {
        MoleculeSpecies[] cueSpecies = cue.Species;

        moleculeSpecies = new MoleculeSpecies[cueSpecies.Length + (nullPossible ? 1 : 0)];

        moleculeSpeciesString = new string[moleculeSpecies.Length];

        int i = 0;

        if (nullPossible)
        {
            moleculeSpecies[i] = null;
            moleculeSpeciesString[i] = "none";
            i++;
        }

        foreach (var item in cueSpecies)
        {
            moleculeSpecies[i] = item;
            moleculeSpeciesString[i] = item.Name;
            i++;
        }
    }

    private string[] moleculeSpeciesString;
    private MoleculeSpecies[] moleculeSpecies;

    private MoleculeSpecies GetSelection(MoleculeSpecies[] species, int index)
    {
        if (index < 0 || index >= species.Length) { return null; }
        return species[index];
    }

    public MoleculeSpecies Popup(MoleculeSpecies selected, bool small)
    {
        int i = ArrayUtility.IndexOf<MoleculeSpecies>(moleculeSpecies, selected);
        if (small)
        { i = EditorGUILayout.Popup(i, moleculeSpeciesString, GUILayout.MaxWidth(40)); }
        else
        { i = EditorGUILayout.Popup(i, moleculeSpeciesString); }
        return GetSelection(moleculeSpecies, i);
    }
}