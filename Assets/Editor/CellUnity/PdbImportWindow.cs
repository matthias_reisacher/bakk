﻿using UnityEngine;
using UnityEditor;
using System.Collections;

public class PdbImportWindow : EditorWindow
{
    private string pdbName = "";

    public static void UserDownload()
    {
        PdbImportWindow window = (PdbImportWindow)EditorWindow.GetWindow<PdbImportWindow>();
    }

    void OnGUI()
    {

        pdbName = EditorGUILayout.TextField("PDB Name", pdbName);

        if (GUILayout.Button("Download Molecule"))
        {
            OnDownloadClick();
            GUIUtility.ExitGUI();
        }
    }

    void OnDownloadClick()
    {
        pdbName = pdbName.Trim();

        //pdbImport.DownloadFile("http://www.rcsb.org/pdb/download/downloadFile.do?fileFormat=pdb&compression=NO&structureId="+WWW.EscapeURL(pdbName));

        //if (pdbImport.DownloadFile("http://test.xa1.at/data/test.pdb")) {
        if (PdbMoleculeImporter.DownloadFile("http://www.rcsb.org/pdb/download/downloadFile.do?fileFormat=pdb&compression=NO&structureId=" + WWW.EscapeURL(pdbName), pdbName))
        {

        }
    }
}