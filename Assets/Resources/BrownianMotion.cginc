float Hash(float n)
{
	return frac(sin(n) * 43758.5453);
}

float Noise3D(float3 x)
{
	// The noise function returns a value in the range -1.0f -> 1.0f

	float3 p = floor(x);
	float3 f = frac(x);

	f = f * f * (3.0 - 2.0 * f);
	float n = p.x + p.y * 57.0 + 113.0 * p.z;

	return lerp(lerp(lerp(Hash(n + 0.0), Hash(n + 1.0), f.x),
	lerp(Hash(n + 57.0), Hash(n + 58.0), f.x), f.y),
	lerp(lerp(Hash(n + 113.0), Hash(n + 114.0), f.x),
	lerp(Hash(n + 170.0), Hash(n + 171.0), f.x), f.y), f.z);
}

struct Brownian
{
	float4 pos;
	float4 rot;
};

Brownian BrownianMotion(in int id, in float4 pos, in float4 rot, in float speedFactor, in float translationScaleFactor, in float rotationScaleFactor, in float time)
{
	float randx = frac(sin(dot(float2(id, pos.x), float2(12.9898, 78.233))) * 43758.5453);
	float randy = frac(sin(dot(float2(id, pos.y), float2(12.9898, 78.233))) * 43758.5453);
	float randz = frac(sin(dot(float2(id, pos.z), float2(12.9898, 78.233))) * 43758.5453);

	float4 tt = float4(time / 20, time, time * 2, time * 3);

	float3 pn;
	pn.x = Noise3D(randx + 100 + pos.xyz + tt.xyz * speedFactor);
	pn.y = Noise3D(randy + 200 + pos.yzx + tt.yzx * speedFactor);
	pn.z = Noise3D(randz + 300 + pos.zxy + tt.zxy * speedFactor);
	pn -= 0.5;

	float4 rn;
	rn.x = Noise3D(randx + 400 + pos.xzy + tt.xyz * speedFactor);
	rn.y = Noise3D(randy + 500 + pos.yxz + tt.yzx * speedFactor);
	rn.z = Noise3D(randz + 600 + pos.zyx + tt.zxy * speedFactor);
	rn.w = Noise3D(randz + 700 + pos.xyx + tt.xyz * speedFactor);
	rn -= 0.5;

	Brownian res;
	res.pos = float4(pos.xyz + pn * translationScaleFactor, pos.w);
	res.rot = normalize(rot + rn * rotationScaleFactor);

	return res;
}