//--------------------------------------------------------------------------------------

float DistanceAtomsToAtoms(in float3 pos1, in float4 rot1, in int atomStart1, in int atomCount1, in float3 pos2, in float4 rot2, in int atomStart2, in int atomCount2)
{
	float smallestDist = 1000.0f;

	for (int i = atomStart1; i < atomStart1 + atomCount1; i++)
	{
		float3 atomPos1 = pos1 + QuaternionTransform(rot1, _Atoms[i].xyz);
		float atomRadius1 = _Atoms[i].w;

		for (int j = atomStart2; j < atomStart2 + atomCount2; j++)
		{
			float3 atomPos2 = pos2 + QuaternionTransform(rot2, _Atoms[j].xyz);
			float atomRadius2 = _Atoms[j].w;

			float dist = distance(atomPos1, atomPos2) - (atomRadius1 + atomRadius2);

			if (dist < smallestDist)
			{
				smallestDist = dist;
			}
		}
	}

	return smallestDist;
}

//--------------------------------------------------------------------------------------

float DistanceBsToAtoms(in float3 pos1, in float radius1, in float3 pos2, in float4 rot2, in int atomStart2, in int atomCount2)
{
	float smallestDist = 1000.0f;

	for (int i = atomStart2; i < atomStart2 + atomCount2; i++)
	{
		float3 atomPos2 = pos2 + QuaternionTransform(rot2, _Atoms[i].xyz);
		float atomRadius2 = _Atoms[i].w;

		float dist = distance(pos1, atomPos2) - (radius1 + atomRadius2);

		if (dist < smallestDist)
		{
			smallestDist = dist;
		}
	}

	return smallestDist;
}

//--------------------------------------------------------------------------------------

float CalculateMovementLength(in float3 diff, in float3 movement, in float sumRadii)
{
	float movementLength = length(movement);
	float dist = length(diff);

	// Calculate closest distance to second sphere on movement-vector
	float cd = dot(normalize(movement), diff);

	// Early escape test: return if not moving towards second sphere.
	if (cd <= 0)	return -1;

	// Calculate distance^2 from cd to center of second sphere
	float f = dist * dist - cd * cd;

	// Eliminate precision error
	if (f < 0.0001)	f = 0.0f;

	float sumRadiiSquared = sumRadii * sumRadii;

	// Escape test: return if closest distance between spheres during movement is larger than the sum of their radii.
	if (f >= sumRadiiSquared)	return -1;

	// We know that the two spheres will collide. Now we want to shrink the movement vector
	// so that sphere 1 moves toward sphere2 as far as possible without colliding.
	float t = sumRadiiSquared - f;	// Amount of adaption

	// Error prevention: This should not happen
	if (t < 0)	return -1;

	// Length of movement vector after adaption
	float newMovementLength = cd - sqrt(t);

	// To make sure that the adaption is not larger than the movement vector
	if (movementLength < newMovementLength)	return -1;

	// Return the length of the adapted movement vector.
	return newMovementLength;
}

//----------------------------------------------------------------------------

float CalculateMovementLengthWithAtoms(in float3 movement, 
	in float3 pos1, in float4 rot1, in int atomStart1, in int atomCount1, 
	in float3 pos2, in float4 rot2, in int atomStart2, in int atomCount2)
{
	float smallestLength = 1000.0f;
	float movementLength = length(movement);

	for (int i = atomStart1; i < atomStart1 + atomCount1; i++)
	{
		float3 atomPos1 = pos1 + QuaternionTransform(rot1, _Atoms[i].xyz);
		float atomRadius1 = _Atoms[i].w;

		for (int j = atomStart2; j < atomStart2 + atomCount2; j++)
		{
			float3 atomPos2 = pos2 + QuaternionTransform(rot2, _Atoms[j].xyz);
			float atomRadius2 = _Atoms[j].w;

			float3 diff = atomPos2 - atomPos1;
			float dist = length(diff);
			float sumRadii = atomRadius1 + atomRadius2;

			// Early escape test: movement width is less than free space between atoms.
			if (movementLength < dist - sumRadii)	continue;

			// Probable collision between atoms -> calculate width of movement vector until collision.
			float newLength = CalculateMovementLength(diff, movement, sumRadii);

			if (newLength != -1 && newLength < smallestLength)
			{
				smallestLength = newLength;
			}
		}
	}

	return smallestLength;
}

//--------------------------------------------------------------------------------------

struct MoleculeCollision
{
	int Id;
	float Ratio;
	float3 SecondMovement;
	float NewLength;
	float SumRadii;
	float Dist;
	float3 RM2;
};

// Returns the ID of the colliding-molecule with the closest collision distance or -1.
MoleculeCollision CellCollision(in int ownId, in int cellId, in float4 pos, in Molecule m, in float3 movement, in int speed, in int fast)
{
	MoleculeCollision res;
	res.Id = -1;
	res.Ratio = 1.0f;
	res.SecondMovement = float3(0, 0, 0);

	int cellSize = _BinCounter[cellId];	// Number of molecules in the cell
	if (cellSize == 0)	return res;

	int cellStart = _PrefixSum[cellId];

	for (int i = cellStart; i < cellStart + cellSize; i++)
	{
		int id = _OrderedIds[i];
		if (id == ownId)	continue;

		Molecule c = _MoleculesRW[id];
		float4 cPos = _MoleculeInstancePositions[c.DataId];

		// Early escape test: no collision if distance is larger than maxMovementWidth * 2 (incl. radii).
		float3 diff = cPos.xyz - pos.xyz;
		float dist = length(diff);

		if (dist > (speed * 2) + pos.w + cPos.w)	continue;

		// Both molecules are moving -> use relative movement vector.
		float3 relativeMovement = movement - c.Movement;
		float sumRadii = pos.w + cPos.w;

		float newLength, ratio;

		// Check if bounding spheres or atoms should be tested
		// If fast collision detection is enabled, use only the bounding spheres.
		if (length(relativeMovement) <= dist - sumRadii || fast == 1)
		{
			/***** Check with bounding spheres *****/
			newLength = CalculateMovementLength(diff, relativeMovement, sumRadii);

			// If no collision was found, continue.
			if (newLength == -1)	continue;
			else
			{
				// Bounding spheres are colliding!
				// Calculate ratio for bounding sphere and shorten movement vector
				ratio = newLength / (float)length(relativeMovement);	// Value between 0 and 1
				
				if (fast != 1)
				{
					// Fast collision detecion is disabled -> check atoms!
					float3 shortenedRelativeMovement = relativeMovement * (1 - ratio);

					float4 rot = _MoleculeInstanceRotations[m.DataId];
					float4 cRot = _MoleculeInstanceRotations[c.DataId];

					// Find shortest movement width to an atom
					newLength = CalculateMovementLengthWithAtoms(shortenedRelativeMovement, pos.xyz + movement * ratio, rot, m.AtomStart, m.AtomCount, cPos.xyz + c.Movement * ratio, cRot, c.AtomStart, c.AtomCount);

					// If the atoms are not colliding, ratio is 1.
					if (newLength != -1)	ratio += newLength / (float)length(relativeMovement);	// Value between 0 and 1
					else	ratio = 1.0f;
				}
			}
		}
		else
		{
			/***** Check with atoms *****/
			float4 rot = _MoleculeInstanceRotations[m.DataId];
			float4 cRot = _MoleculeInstanceRotations[c.DataId];

			// Find shortest movement width to an atom
			newLength = CalculateMovementLengthWithAtoms(relativeMovement, pos.xyz, rot, m.AtomStart, m.AtomCount, cPos.xyz, cRot, c.AtomStart, c.AtomCount);

			// If no collision was found, continue.
			if (newLength == -1)	continue;

			// Calculate ratio for atoms
			ratio = newLength / (float)length(relativeMovement);	// Value between 0 and 1
		}

		if (ratio < res.Ratio)
		{
			res.Id = id;
			res.Ratio = ratio;
			res.SecondMovement = c.Movement;
		}
	}

	return res;
}

//----------------------------------------------------------------------------

MoleculeCollision CompareDynamics(MoleculeCollision a, MoleculeCollision b)
{
	if (b.Id == -1)	return a;
	else
	{
		if (a.Id == -1)	return b;
		else if (a.Ratio <= b.Ratio)	return a;
		else	return b;
	}
}

MoleculeCollision DynamicCollisionDetectionWithMolecules(in int ownId, in float4 pos, in Molecule m, in float3 movement, in float speed, in int fast)
{
	// Check if the molecule resided in multiple cells
	int range = pos.w + speed;
	bool previousCellX = range > m.Offset.x;
	bool nextCellX = _CellWidth < range + m.Offset.x;
	bool previousCellY = range > m.Offset.y;
	bool nextCellY = _CellWidth < range + m.Offset.y;
	bool previousCellZ = range > m.Offset.z;
	bool nextCellZ = _CellWidth < range + m.Offset.z;

	// To avoid a long compiled code, an array is created to allow CellCollision in a loop!
	int cellCheck[27];
	for (int i = 0; i < 27; i++)
	{
		cellCheck[i] = -1;
	}

	// Check for collision in the main-cell (where the center of the molecule is).
	cellCheck[0] = m.CellId;

	// Check in adjacent cells
	if (previousCellX)
	{
		cellCheck[1] = m.CellId - 1;

		if (previousCellY)
		{
			cellCheck[2] = m.CellId - 1 - _NumCells;

			if (previousCellZ)	cellCheck[3] = m.CellId - 1 - _NumCells - _NumCells * _NumCells;
			else if (nextCellZ)	cellCheck[4] = m.CellId - 1 - _NumCells + _NumCells * _NumCells;
		}
		else if (nextCellY)
		{
			cellCheck[5] = m.CellId - 1 + _NumCells;

			if (previousCellZ)	cellCheck[6] = m.CellId - 1 + _NumCells - _NumCells * _NumCells;
			else if (nextCellZ)	cellCheck[7] = m.CellId - 1 + _NumCells + _NumCells * _NumCells;
		}

		if (previousCellZ)	cellCheck[8] = m.CellId - 1 - _NumCells * _NumCells;
		else if (nextCellZ)	cellCheck[9] = m.CellId - 1 + _NumCells * _NumCells;
	}
	else if (nextCellX)
	{
		cellCheck[10] = m.CellId + 1;

		if (previousCellY)
		{
			cellCheck[11] = m.CellId + 1 - _NumCells;

			if (previousCellZ)	cellCheck[12] = m.CellId + 1 - _NumCells - _NumCells * _NumCells;
			else if (nextCellZ)		cellCheck[13] = m.CellId + 1 - _NumCells + _NumCells * _NumCells;
		}
		else if (nextCellY)
		{
			cellCheck[14] = m.CellId + 1 + _NumCells;

			if (previousCellZ)		cellCheck[15] = m.CellId + 1 + _NumCells - _NumCells * _NumCells;
			else if (nextCellZ)			cellCheck[16] = m.CellId + 1 + _NumCells + _NumCells * _NumCells;
		}

		if (previousCellZ)	cellCheck[17] = m.CellId + 1 - _NumCells * _NumCells;
		else if (nextCellZ)	cellCheck[18] = m.CellId + 1 + _NumCells * _NumCells;
	}

	if (previousCellY)
	{
		cellCheck[19] = m.CellId - _NumCells;

		if (previousCellZ)		cellCheck[20] = m.CellId - _NumCells - _NumCells * _NumCells;
		else if (nextCellZ)	cellCheck[21] = m.CellId - _NumCells + _NumCells * _NumCells;
	}
	else if (nextCellY)
	{
		cellCheck[22] = m.CellId + _NumCells;

		if (previousCellZ)	cellCheck[23] = m.CellId + _NumCells - _NumCells * _NumCells;
		else if (nextCellZ)		cellCheck[24] = m.CellId + _NumCells + _NumCells * _NumCells;
	}

	if (previousCellZ)
	{
		cellCheck[25] = m.CellId - _NumCells * _NumCells;
	}
	else if (nextCellZ)
	{
		cellCheck[26] = m.CellId + _NumCells * _NumCells;
	}

	// Do CollisionDetection for all Cells in range of the molecule and return the closest collision
	MoleculeCollision d = CellCollision(ownId, cellCheck[0], pos, m, movement, speed, fast);
	for (int i = 1; i < 27; i++)
	{
		d = CompareDynamics(d, CellCollision(ownId, cellCheck[i], pos, m, movement, speed, fast));
	}

	return d;
}

//----------------------------------------------------------------------------

struct  ProteinCollision
{
	int ProteinId;
	float3 Movement;
};

ProteinCollision DynamicCollisionDetectionWithProteins(in float4 pos, in float4 rot, in int atomStart, in int atomCount, in float3 movement, in float maxMovementLength, in int fast)
{
	ProteinCollision c;
	c.ProteinId = -1;
	c.Movement = movement;

	float3 newPos = pos.xyz + movement;
	float movementLength = length(movement);

	// If brownian motion is disabled, movementLength = maximum movementlength
	if (movementLength == 0.0f)	movementLength = maxMovementLength;

	for (int i = 0; i < _Indices[0].Protein; i++)
	{
		Protein p = _Proteins[i];

		float4 pPos = _ProteinPositions[p.Id];
		float4 pRot = _ProteinRotations[p.Id];

		float boundingDist = distance(newPos, pPos.xyz);

		if (boundingDist <= pos.w + pPos.w)
		{
			// Possible collision with protein found -> check atoms
			float dist = 0.0f;

			// If fast collision detection is enabled, use only the molecule's bounding sphere.
			if (fast == 1)
			{
				dist = DistanceBsToAtoms(newPos, pos.w, pPos.xyz, pRot, p.AtomStart, p.AtomCount);
			}
			else
			{
				dist = DistanceAtomsToAtoms(newPos, rot, atomStart, atomCount, pPos.xyz, pRot, p.AtomStart, p.AtomCount);
			}

			if (dist < 0.0f)
			{
				//Collision found -> calculate new movement vector
				c.ProteinId = p.Id;
				c.Movement = normalize(newPos - pPos.xyz) * movementLength;
				return c;
			}
		}
	}

	return c;
}

//----------------------------------------------------------------------------

float4 StaticCollisionDetectionWithMolecules(in float4 pos, in float3 direction)
{
	int limit = 0;	// To prevent infinite loops
	int i = 0;
	while (i < _TransferedData[0] && limit < 10)
	{
		Molecule c = _MoleculesRW[i];
		// Ignore deleted molecules
		if (c.DataId < 0)
		{
			i++;
			continue;
		}

		float4 cPos = _MoleculeInstancePositions[c.DataId];

		float3 diff = pos.xyz - cPos.xyz;
		int sumRadii = pos.w + cPos.w;
		float diffLength = length(diff);

		if (diffLength < sumRadii)
		{
			/***** Collision found -> find new position and check all over again *****/
			// To prevent normalizing a zero vector.
			if (diffLength == 0)	diff = RandGenF3(cPos.x + cPos.y + cPos.z + cPos.w);

			// Calculate offset
			if (length(direction) == 0)
			{
				// No direction is given -> find closest position outside the colliding molecule
				pos.xyz += normalize(diff) * (sumRadii - diffLength);
			}
			else
			{
				// Direction id given -> move molecule along the direction vector
				pos.xyz += direction * sumRadii;
			}

			i = 0;
			limit++;
		}
		else
		{
			i++;
		}
	}

	return pos;
}

//----------------------------------------------------------------------------

float4 StaticCollisionDetectionWithProteins(in float4 pos)
{
	int limit = 0;	// To prevent infinite loops
	int i = 0;
	while (i < _Indices[0].Protein && limit < 1)
	{
		Protein p = _Proteins[i];
		float4 pPos = _ProteinPositions[p.Id];

		float boundingDist = distance(pos.xyz, pPos.xyz);

		if (boundingDist <= pos.w + pPos.w)
		{
			// Possible collision with protein found -> check atoms
			float dist = DistanceBsToAtoms(pos.xyz, pos.w, pPos.xyz, _ProteinRotations[p.Id], p.AtomStart, p.AtomCount);

			if (dist < 0.0f)
			{
				// Collision found -> find new position and check all over again
				float3 diff = pos.xyz - pPos.xyz;
				float lengthDiff = length(diff);
				
				if (lengthDiff == 0)	pos.xyz += RandGenF3(pos.x + pos.y + pos.z + pos.w) * (pos.w + pPos.w);
				else	pos.xyz += normalize(diff) * (pos.w + pPos.w - lengthDiff);
				
				i = 0;
				limit++;
			}
			else
			{
				i++;
			}
		}
		else
		{
			i++;
		}
	}

	return pos;
}

//----------------------------------------------------------------------------

float4 FindEmptySpace(in float3 pos, in float radius)
{
	return StaticCollisionDetectionWithMolecules(float4(pos.x, pos.y, pos.z, radius), float3(0, 0, 0));
}