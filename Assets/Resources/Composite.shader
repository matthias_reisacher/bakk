﻿Shader "Custom/Composite" 
{
	Properties {
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	}

	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		
		Pass	// 0
		{
			ZWrite On
			ZTest Always

			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			sampler2D_float _CameraDepthTexture;

			void frag(v2f_img i,  out float4 color : COLOR0, out float depth : DEPTH) 
			{             
				color = tex2D(_MainTex, i.uv);       
				depth = tex2D(_CameraDepthTexture, i.uv);
			}
			ENDCG
		}

		Pass	// 1
		{
			ZWrite On
			ZTest Lequal

			CGPROGRAM
			
			#pragma fragment frag
			#pragma vertex vert_img

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			sampler2D_float _CameraDepthTexture;

			sampler2D _ColorTexture;
			sampler2D_float _DepthTexture;

			sampler2D _Glow;

			sampler2D _LineVisColor;
			sampler2D_float _LineVisDepth;

			void frag(v2f_img i,  out float4 color : COLOR0, out float depth : depth) 
			{   
				float proteinDepth = (tex2D(_DepthTexture, i.uv));
				float cameraDepth = (tex2D(_CameraDepthTexture, i.uv));
				float lineDepth = (tex2D(_LineVisDepth, i.uv));
				
				float4 glowColor = (tex2D(_Glow, i.uv));

				// Test between protein and line textures
				bool depthTest = proteinDepth < lineDepth;
				float4 customColor = depthTest ? tex2D(_ColorTexture, i.uv) : tex2D(_LineVisColor, i.uv);
				float customDepth = depthTest ? proteinDepth : lineDepth;

				// Test between resulting texture and camera/background
				depthTest = customDepth < cameraDepth;
				color = depthTest ? customColor : float4(tex2D(_MainTex, i.uv).xyz, 1.0f);
				depth = depthTest ? customDepth : cameraDepth;
				
				// Process glow effect
				color.xyz += glowColor.xyz * glowColor.w;
			}
			ENDCG
		}

		Pass	// 2
		{
			ZTest Always

			CGPROGRAM
			#pragma fragment frag
			#pragma vertex vert_img
						
			#pragma target 5.0	
			#pragma only_renderers d3d11			

			#include "UnityCG.cginc"
			
			Texture3D<float> _HiZMap;

			void frag(v2f_img i, out float4 color : COLOR0) 
			{       
				uint2 coord = uint2(i.uv.x * _ScreenParams.x, i.uv.y * _ScreenParams.y);   
				color.r = Linear01Depth(_HiZMap[uint3(coord, 4)]); 
			}
			
			ENDCG
		}

		Pass	// 3
		{
			ZTest Always

			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
					
			#pragma target 5.0	
			#pragma only_renderers d3d11

			#include "UnityCG.cginc"
			#include "Helper.cginc"	

			uniform Texture2D<int> _IdTexture;
			StructuredBuffer<float4> _ProteinColors;
			StructuredBuffer<float4> _ProteinInstanceInfo;

			void frag(v2f_img i, out float4 color : COLOR0) 
			{   
				int2 uv = i.uv * _ScreenParams.xy; 
				int id = _IdTexture[uv];

				if(id >= 0)
				{
					float4 proteinInfo = _ProteinInstanceInfo[id];

					// If InstanceState is Null, this is the resulting color
					color = float4(0, 0, 0, proteinInfo.w);

					if (proteinInfo.y == 0 || proteinInfo.y == 1)
					{
						// InstanceState is Normal or highlighted
						float4 proteinColor = _ProteinColors[proteinInfo.x];
						color.xyz = ColorCorrection(proteinColor.xyz);

						if (proteinInfo.y == 1)
						{
							// InstanceState is Highlighted
							float selectedBrightness = 0.5;
							color.xyz += float3(selectedBrightness, selectedBrightness, selectedBrightness);
						}
					}
					
					if(proteinInfo.y == 2)
					{
						// InstanceState is set to Grayscale
						float gray = 0.7;
						color.xyz = float3(gray, gray, gray);
					}
				}
				else
				{
					discard;
				}
			}
			
			ENDCG
		}

		Pass	// 4
		{
			ZTest Always

			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag			
			#pragma target 5.0		

			#include "UnityCG.cginc"
			
			sampler2D_float _DepthTexture;

			void frag(v2f_img i, out float4 color : COLOR0) 
			{       
				//uint2 coord = uint2(i.uv.x * _ScreenParams.x, i.uv.y * _ScreenParams.y);   
				color.r = Linear01Depth(tex2D(_DepthTexture, i.uv)); 
			}
			
			ENDCG
		}

		Pass	// 5
		{
			// Transparent version of Pass 1 (without line visualization and glow effect)
			ZWrite On
			ZTest Lequal

			Tags{ "RenderType" = "Transparent" "Queue" = "Transparent" }
			Blend SrcAlpha OneMinusSrcAlpha

			CGPROGRAM

			#pragma fragment frag
			#pragma vertex vert_img

			#include "UnityCG.cginc"

			sampler2D _MainTex;
			sampler2D_float _OpaqueDepthTexture;

			sampler2D _ColorTexture;
			sampler2D_float _DepthTexture;

			void frag(v2f_img i,  out float4 color : COLOR0, out float depth : depth)
			{
				float customDepth = (tex2D(_DepthTexture, i.uv));
				float cameraDepth = (tex2D(_OpaqueDepthTexture, i.uv));

				bool depthTest = customDepth < cameraDepth;
				color = depthTest ? tex2D(_ColorTexture, i.uv) : float4(tex2D(_MainTex, i.uv).xyz, 0.0f);
				depth = depthTest ? customDepth : cameraDepth;
			}
			ENDCG
		}
	}	

	FallBack "Diffuse"
}