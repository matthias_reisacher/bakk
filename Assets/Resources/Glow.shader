﻿Shader "Custom/Glow" 
{
	Properties {
		_MainTex ("Base (RGB) Trans (A)", 2D) = "white" {}
	}

	SubShader 
	{
		Tags { "RenderType"="Opaque" }
		
		// Pass = 0
		Pass 
		{
			ZTest Always

			CGPROGRAM
			#pragma vertex vert_img
			#pragma fragment frag
					
			#pragma target 5.0	
			#pragma only_renderers d3d11

			#include "UnityCG.cginc"
			#include "Helper.cginc"	

			uniform Texture2D<int> _IdTexture;
			StructuredBuffer<float4> _ProteinColors;
			StructuredBuffer<float4> _ProteinInstanceInfo;
			StructuredBuffer<float2> _ProteinAdditionalInfos;
			StructuredBuffer<float2> _MoleculeAdditionalInfos;

			uniform int _NumProteinInstances;

			void frag(v2f_img i, out float4 color : COLOR0) 
			{   
				int2 uv = i.uv * _ScreenParams.xy; 
				int id = _IdTexture[uv];

				if(id >= 0)
				{
					float glowAlpha = 0.0f;

					// Determine if ingredient is a protein or a molecule and get the glowAlpha-value from the respective buffer.
					if (id < _NumProteinInstances)	glowAlpha = _ProteinAdditionalInfos[id].x;
					else	glowAlpha = _MoleculeAdditionalInfos[id - _NumProteinInstances].x;

					// Discard ingredients which are not glowing
					if (glowAlpha <= 0)	discard;

					float4 info = _ProteinInstanceInfo[id];
					color = float4(ColorCorrection(_ProteinColors[info.x].xyz), glowAlpha);
				}
				else
				{
					discard;
				}
			}
			
			ENDCG
		}
	}	

	FallBack "Diffuse"
}