//--------------------------------------------------------------------------------------

// Takes the first molecule with the specified speciesId from Molecules and returns the index of the selected molecule.
int FindRandomMolecule(in int speciesId)
{
	int numMolecules = _TransferedData[0];
	
	for (int i = 0; i < numMolecules; i++)
	{
		Molecule c = _MoleculesRW[i];

		// Ignore deleted molecules
		if (c.DataId < 0)	continue;

		if (c.IngredientId == speciesId && c.ReactionId == -1)
		{
			return i;
		}
	}

	return -1;
}

// Takes the closest molecule with the specified speciesID to the forwarded position from Molecules and returns the index.
int FindClosestMolecule(in int speciesId, in float3 position)
{
	float minDistance = 100000;	// TODO replace with max value of float
	int minId = -1;

	int numMolecules = _TransferedData[0];
	
	for (int i = 0; i < numMolecules; i++)
	{
		Molecule c = _MoleculesRW[i];
		
		// Ignore deleted molecules
		if (c.DataId < 0)	continue;

		if (c.IngredientId == speciesId && c.ReactionId == -1)
		{
			
			// Calculate distance and check with minimum
			float dist = distance(position, _MoleculeInstancePositions[c.DataId].xyz);

			if (dist < minDistance)
			{
				minDistance = dist;
				minId = i;
			}
		}
	}

	return minId;
}

//--------------------------------------------------------------------------------------

struct BsPos
{
	float bs;
	float3 pos;
	int enableVisLines;
};

// Adds the last reaction to the molecule with the specified ID and returns the molecule's position.
BsPos AddMoleculeToReaction(in int moleculeCId, in int proteinId, in int createVisLine)
{
	Molecule c = _MoleculesRW[moleculeCId];
	_MoleculesRW[moleculeCId].ReactionId = _Indices[0].Reaction;	// Add reaction to molecule
	_MoleculesRW[moleculeCId].ProteinId = proteinId;	// Add proteinId to molecule
	InterlockedAdd(_TransferedData[c.IngredientId - _FirstSpeciesId + _CounterOffset], -1);	// Decrease species-counter
	_ReagentsAppend.Append(moleculeCId);	// Add molecule-id to reagents
	_Indices[0].Reagent += 1;	// increase reagent-counter
	
	int visLine = 0;
	if (createVisLine == 1)
	{
		_MoleculesRW[moleculeCId].VisLineInfo = 1;	// Create static visline
		_MoleculesRW[moleculeCId].VisLineOldReactionId = -1;
		visLine = 1;
	}
	else
	{
		// Check if molecule is already part of a vis line
		if (_MoleculesRW[moleculeCId].VisLineOldReactionId != -1)
			visLine = 1;
	}

	BsPos res;
	res.pos = _MoleculeInstancePositions[c.DataId].xyz;
	res.bs = _MoleculeInstancePositions[c.DataId].w;
	res.enableVisLines = visLine;

	return res;
}

//--------------------------------------------------------------------------------------

float3 RandGenF3(in int n)
{
	int r = n % 3;
	
	if (r == 0)	return float3(1, 0, 0);
	else if (r == 1)	return float3(0, 1, 0);
	else	return float3(0, 0, 1);
}

//--------------------------------------------------------------------------------------
