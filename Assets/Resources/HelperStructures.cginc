struct Data
{
	float4 Info;
	float4 Position;
	float4 Rotation;
};

struct Molecule
{
	int DataId;					// _MoleculeInstance-ID
	int IngredientId;			// _MoleculeInstanceInfos.x
	int ReactionId;				// _Reactions
	int CellId;					// Cellid
	float3 Offset;				// Offset from molecule center to cell-beginning
	int Ready;					// True if molecule is ready for reaction.
	int HasMoved;				// True if the molecule has already been moved.
	int ProteinId;				// If the assigned reaction needs a protein for completion, ProteinId contains the protein's ID. Otherwise -1.
	int AtomCount;
	int AtomStart;
	float3 Movement;			// The movement vector for the next update-step.
	float3 OldPosition;			// The position befor the next update-step is applied.
	int VisLineInfo;			// Defines if a new visualization line should be created the line-type (static or dynamic).
								// -1: not part of a line -> ignore it.
								//  0: Create new dynamic line (after creation, VisLineInfo is set to 0).
								//  1: Create new static line (after creation, VisLineInfo is set to -1).
								//  2: Already part of a visline -> update VisLine.Molecule-position. 
	int VisLineOldReactionId;	// Reaction-ID for products.
};

struct Reaction
{
	int NumReagents;
	int NumProducts;
	int ReagentId;				// _ReagentProductIds
	int ProductId;				// _ReactionProductIds
	float3 Position;
	int ProteinId;				// If the assigned reaction needs a protein for completion, ProteinId contains the protein's ID. Otherwise -1.
	int VisLineEnabled;			// If visualization lines for the products should be created.
	int pad0; int pad1; int pad2;	// To align structure size to a 128-bit stride
};

struct Product
{
	int Id;
	float Radius;
};

struct Indices
{
	int Reaction;				// _Reactions
	int Reagent;				// _ReactionReagentIds
	int Product;				// _ReactionProductIds
	int Collision;				// _Collisions
	int PerformReaction;		// _PerformReactions
	int Protein;				// _Proteins
	int FreeIndices;			// _FreeIndices
};

struct Collision
{
	int Id1;
	int	Id2;
	float3 Movement1;
	float3 Movement2;
	float Ratio;
	int pad0; int pad1; int pad2;	// To align structure size to a 128-bit stride
};

struct Protein
{
	int Id;
	int AtomCount;
	int AtomStart;
	int pad0;	// To align structure size to a 128-bit stride
};

struct VisLine
{
	int MoleculeId;				// id to update a dynamic visualization line.
	int ReactionId;				// to set a dynamic line to the correct position when the molecule has reacted.
	float3 Molecule;
	float3 Target;
	float4 Color;				// color of the visualization line.
};
