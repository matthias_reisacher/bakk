﻿Shader "Custom/LineRenderer"
{
	SubShader
	{
		Pass	// 0
		{
			ZWrite On
			ZTest Lequal

			CGPROGRAM
			#pragma vertex vert
			#pragma geometry geom
			#pragma fragment frag

			#pragma only_renderers d3d11
			#pragma target 5.0	

			#include "UnityCG.cginc"

			struct Line
			{
				int MoleculeId;
				int ReactionId;
				float3 Molecule;
				float3 Target;
				float4 Color;
			};

			float _Scale;
			int _UseFixedColor;
			float4 _FixedColor;
			StructuredBuffer<Line> _LineInfo;


			struct gs_input
			{
				int id : INT0;
			};

			struct fs_input
			{
				centroid float4 pos : SV_Position;
				nointerpolation float3 color : FLOAT30;
			};

			void vert(uint id : SV_VertexID, out gs_input output)
			{
				output.id = id;
			}

			[maxvertexcount(2)]
			void geom(point gs_input input[1], inout LineStream<fs_input> lineStream)
			{
				Line l = _LineInfo[input[0].id];
				fs_input output;
				
				output.pos = mul(UNITY_MATRIX_MVP, float4(l.Molecule.xyz * _Scale, 1));
				output.color = l.Color.xyz;
				lineStream.Append(output);

				output.pos = mul(UNITY_MATRIX_MVP, float4(l.Target.xyz * _Scale, 1));
				output.color = l.Color.xyz;
				lineStream.Append(output);

				lineStream.RestartStrip();
			}

			void frag(fs_input input, out float4 color : COLOR0, out float depth : DEPTH)
			{
				if (_UseFixedColor == 0)	color = float4(input.color, 1);
				else	color = _FixedColor;

				// Find depth
				float eyeDepth = LinearEyeDepth(input.pos.z);
				depth = 1 / (eyeDepth * _ZBufferParams.z) - _ZBufferParams.w / _ZBufferParams.z;
			}
			ENDCG
		}
	}
}
