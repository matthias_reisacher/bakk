﻿#include "../Helper.cginc"

#define MAX_SPHERE_COUNT_PER_BATCH 4096

uniform	float _Scale;
uniform int _EnableLod;
uniform	int _NumLevels;
uniform	int _NumInstances;
uniform float4 _CameraForward;
uniform float4 _CameraPosition;

uniform	int _CullFlagFilter;
uniform float4 _FrustrumPlanes[6];
uniform int _CombinedBufferOffset;

uniform	StructuredBuffer<int> _ProteinAtomCount;
uniform	StructuredBuffer<int> _ProteinAtomStart;
uniform	StructuredBuffer<int> _ProteinClusterCount;
uniform	StructuredBuffer<int> _ProteinClusterStart;

uniform	StructuredBuffer<int> _ProteinVisibilityFlag;
uniform StructuredBuffer<int> _CullFlags;

uniform StructuredBuffer<float4> _LodLevelsInfos;
uniform	RWStructuredBuffer<float4> _Infos;
uniform	StructuredBuffer<float4> _Positions;
uniform	StructuredBuffer<float4> _Rotations;
uniform RWStructuredBuffer<float2> _AdditionalInfos;

uniform AppendStructuredBuffer<int4> _OpaqueSphereBatchInfos;
uniform AppendStructuredBuffer<int4> _TransparentSphereBatchInfos;

uniform RWStructuredBuffer<float4> _CombinedInstanceInfos;
uniform RWStructuredBuffer<float4> _CombinedInstancePositions;
uniform RWStructuredBuffer<float4> _CombinedInstanceRotations;

uniform int _EnableConeVisualization;
uniform float _ConeAngle;
uniform float4 _ConeApex;
uniform float3 _ConeBase;
uniform	StructuredBuffer<float4> _OriginalProteinPosition;
uniform int _SelectedId;

uniform int _PauseGlow;

/******************************************************************************/

#pragma kernel ProteinFillBatchBuffer

[numthreads(1, 1, 1)]
void ProteinFillBatchBuffer(uint3 id : SV_DispatchThreadID)
{
	if (id.x >= (uint)_NumInstances) return;
	
	float4 infos = _Infos[id.x];
	float4 sphere = _Positions[id.x] * _Scale;

	// Ignore invalid ingredients
	if (infos.x < 0)	return;
	
	// Ignore culled ingredients
	int cullFlag = _CullFlags[id.x];
	if (cullFlag == 1)	return;

	// Determine alpha value with cone test
	float alpha = infos.w;
	if (_EnableConeVisualization == 1 && _CombinedBufferOffset == 0)
	{
		if (id.x == _SelectedId)	alpha = 1.0f;
		else	alpha = SphereConeTest(_ConeApex.xyz * _Scale, _ConeBase.xyz, _OriginalProteinPosition[id.x] * _Scale, _ConeAngle, _ConeAngle * 2.0f);

		float stepSize = 0.05f;
		float alphaDiff = infos.w - alpha;
		if (alphaDiff > 0)	infos.w -= (alphaDiff > stepSize) ? stepSize : alphaDiff;
		if (alphaDiff < 0)	infos.w += (alphaDiff < -stepSize) ? stepSize : -alphaDiff;
		infos.w = clamp(infos.w, 0.0f, 1.0f);
	}
	else
		infos.w = 1.0f;
	
	_Infos[id.x] = infos;

	bool frustrumTest = SphereFrustrumTest(_FrustrumPlanes, sphere);

	//if(_CullFlagFilter == -1 && cullFlag == 0) return;
	//if(_CullFlagFilter != -1 && cullFlag != _CullFlagFilter) return;
	if (frustrumTest || infos.w == 0.0f || _ProteinVisibilityFlag[infos.x] == 0) return;

	int lodLevel = 0;
	float cameraDistance = dot(sphere.xyz - _CameraPosition.xyz, _CameraForward.xyz);

	// Compute lod level from lod infos
	for (int i = 0; i < _NumLevels; i++) { lodLevel = (cameraDistance < _LodLevelsInfos[i].x) ? lodLevel : i + 1; }

	lodLevel = _EnableLod == 0 ? 0 : lodLevel;
	int clusterLevel = max(lodLevel - 1, 0);

	// Find the count / start index of the protein
	int sphereCount = (lodLevel == 0) ? _ProteinAtomCount[infos.x] : _ProteinClusterCount[infos.x * _NumLevels + clusterLevel];
	int sphereStart = (lodLevel == 0) ? _ProteinAtomStart[infos.x] : _ProteinClusterStart[infos.x * _NumLevels + clusterLevel];

	// Find the number of batches for this protein
	int numBatches = ceil((float)sphereCount / (float)MAX_SPHERE_COUNT_PER_BATCH);
	int sphereCountPerBatch = ceil(float(sphereCount) / float(numBatches));

	int batchStart = 0;
	int batchCount = sphereCountPerBatch;

	for (int j = 0; j < numBatches; j++)
	{
		batchCount = min(batchStart + sphereCountPerBatch, sphereCount) - batchStart;
		int4 sphereBatch = int4(id.x + _CombinedBufferOffset, lodLevel, batchCount, sphereStart + batchStart);
		
		// Append the batch either to the opaque-buffer or the transparent-buffer.
		if(infos.w >= 1.0f)
			_OpaqueSphereBatchInfos.Append(sphereBatch);
		else
			_TransparentSphereBatchInfos.Append(sphereBatch);
		
		batchStart += batchCount;
	}

	float glowAlpha = _AdditionalInfos[id.x].x;
	// Decrease glow value if simulation is not paused
	if (glowAlpha > 0 && _PauseGlow != 1)
	{
		glowAlpha -= 0.05f;
		_AdditionalInfos[id.x].x = glowAlpha;
	}
	
	// Combine protein and molecule buffers
	_CombinedInstanceInfos[id.x + _CombinedBufferOffset] = infos;
	_CombinedInstancePositions[id.x + _CombinedBufferOffset] = _Positions[id.x];
	_CombinedInstanceRotations[id.x + _CombinedBufferOffset] = _Rotations[id.x];
}