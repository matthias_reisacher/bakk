﻿using UnityEngine;
using UnityEditor;

[ExecuteInEditMode]
public class CUE : MonoBehaviour
{
    private CUEdata _data;

    // Holds the Instance of CUE
    private static CUE _instance = null;
    public static CUE Instance
    {
        get
        {
            if (_instance != null)
            {
                _instance.OnUnityReload();
                return _instance;
            }

            _instance = FindObjectOfType<CUE>();
            if (_instance == null)
            {
                var go = GameObject.Find("_CUE");
                if (go != null) DestroyImmediate(go);

                go = new GameObject("_CUE") { hideFlags = HideFlags.HideInInspector };
                _instance = go.AddComponent<CUE>();
            }

            _instance.OnUnityReload();

            return _instance;
        }
    }

    private CUE()
    {
        _instance = this;
    }

    private void OnUnityReload()
    {
        if (_data != null)  return;
        
        // Load serialized data if existing or create new one
        //Debug.Log("try load CUE...");
        Object[] assets = Resources.LoadAll("data/CUE");
        //Debug.Log("loaded: " + (assets == null ? "null" : assets.ToString()));
        for (int i = 0; i < assets.Length; i++)
        {
            //Debug.Log("assets["+i+"].Type = " + assets[i].GetType().ToString());
            if (assets[i].GetType().ToString() == "CUEdata") _data = assets[i] as CUEdata;
        }
        if (_data == null)
        {
            //Debug.Log("creating new CUE...");
            _data = ScriptableObject.CreateInstance<CUEdata>();
            AssetDatabase.CreateAsset(_data, "Assets/Resources/data/CUE.asset");
            AssetDatabase.SaveAssets();
        }
        else
        {
            // Loaded species have to be passed to sceneManager to enable molecule creation.
            for (int i = 0; i < assets.Length; i++)
            {
                if (assets[i].GetType().ToString() == "MoleculeSpecies")
                {
                    MoleculeSpecies species = assets[i] as MoleculeSpecies;
                    PdbMoleculeImporter.ReloadMoleculeIngredient(species);
                }
            }
        }
    }

    public MoleculeSpecies[] Species
    {
        get { return _data.Species.ToArray(); }
    }
    public ReactionType[] ReactionTypes
    {
        get { return _data.ReactionTypes.ToArray(); }
    }
    // Time in seconds that is simulated every step
    public float SimulationStep
    {
        get { return _data.SimulationStep; }
        set { _data.SimulationStep = value; EditorUtility.SetDirty(_data); }
    }
    // Duration in seconds of a simulation step in real time (in the visualization)
    public float VisualizationStep
    {
        get { return _data.VisualizationStep; }
        set { _data.VisualizationStep = value; EditorUtility.SetDirty(_data); }
    }
    // Field of Volume Property
    public float Volume
    {
        get { return _data.Volume; }
        set { _data.Volume = value; EditorUtility.SetDirty(_data); }
    }
    // The radius in nm defining a sphere around the center point (defined in Location) in which the molecules are placed.
    public int Radius
    {
        get { return _data.Radius; }
        set { _data.Radius = value; EditorUtility.SetDirty(_data); }
    }
    // Field of Cone Angle
    public int ConeAngle
    {
        get { return _data.coneAngle; }
        set { if (value > 0 && value < 90) _data.coneAngle = value; }
    }

    public void AddSpecies(MoleculeSpecies s)
    {
        _data.Species.Add(s);
        UnityEditor.AssetDatabase.AddObjectToAsset(s, _data);
        EditorUtility.SetDirty(_data);
    }

    public void RemoveSpecies(MoleculeSpecies s)
    {
        _data.Species.Remove(s);
        EditorUtility.SetDirty(_data);
    }

    public void SetMolecules()
    {
        SceneManager.Instance.UploadMoleculeInstanceData();
    }

    public void RemoveMolecules()
    {
        SceneManager.Instance.ClearMolecules();
        SceneManager.Instance.UploadMoleculeInstanceData();
    }

    public void AddReaction(ReactionType r)
    {
        _data.ReactionTypes.Add(r);
        UnityEditor.AssetDatabase.AddObjectToAsset(r, _data);
        EditorUtility.SetDirty(_data);
    }

    public void RemoveReaction(ReactionType r)
    {
        _data.ReactionTypes.Remove(r);
        EditorUtility.SetDirty(_data);
    }
}