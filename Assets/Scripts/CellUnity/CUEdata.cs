﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[System.Serializable]
public class CUEdata : ScriptableObject
{
    public List<MoleculeSpecies> Species;
    public List<ReactionType> ReactionTypes;
    // Time in seconds that is simulated every step
    public float SimulationStep = 0.1f;
    // Duration in seconds of a simulation step in real time (in the visualization)
    public float VisualizationStep = 0.1f;
    // Field of Volume Property
    public float Volume = 1e-14f;
    // The radius in nm defining a sphere around the center point (defined in Location) in which the molecules are placed.
    public int Radius = 300;
    // Half of the radius from the visualization cone.
    public int coneAngle = 20;

    public void OnEnable()
    {
        if (Species == null)    Species = new List<MoleculeSpecies>();
        if (ReactionTypes == null)  ReactionTypes = new List<ReactionType>();
    }
}
