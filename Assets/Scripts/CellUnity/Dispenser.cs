﻿using UnityEngine;

public class Dispenser : MonoBehaviour
{
    // Declare the dispenser as a singleton
    private static Dispenser _instance = null;
    public static Dispenser Instance
    {
        get
        {
            if (_instance != null) return _instance;

            _instance = FindObjectOfType<Dispenser>();
            if (_instance == null)
            {
                var go = GameObject.Find("_Dispenser");
                if (go != null) DestroyImmediate(go);

                go = new GameObject("_Dispenser") { hideFlags = HideFlags.HideInInspector };
                _instance = go.AddComponent<Dispenser>();
            }

            return _instance;
        }
    }

    /// <summary>
    /// The center point where the molecules are placed.
    /// </summary>
    public Vector3 Location = Vector3.zero;

    /// <summary>
    /// Adds a specific amount of molecules to sceneManager.
    /// </summary>
    /// <param name="species">Species to place</param>
    /// <param name="count">Quantity of that species to place</param>
    public void AddMolecules(MoleculeSpecies species, int count)
    {
        Vector3 boxMin = SceneManager.Instance.MinInstancePosition;
        Vector3 boxMax = SceneManager.Instance.MaxInstancePosition;
        Vector3 boxWidth = boxMax - boxMin;

        for (int i = 0; i < count; i++)
        {
            // Get random location
            Vector3 position = new Vector3(Random.value * boxWidth.x, Random.value * boxWidth.y, Random.value * boxWidth.z);
            SceneManager.Instance.AddMoleculeInstance(species.Name, position + boxMin, Random.rotation);
            // Testfunctionality, to distribute the molecules only inside of the compartment.
            /*Vector3 position = Location + Random.insideUnitSphere * (CUE.Instance.Radius - species.Size);
            SceneManager.Instance.AddMoleculeInstance(species.Name, position, Random.rotation);*/
        }
    }
}
