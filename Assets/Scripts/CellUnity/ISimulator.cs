﻿using UnityEngine;
using System.Collections;

/// <summary>
/// Simulator Interface
/// </summary>
public interface ISimulator : System.IDisposable
{
    /// <summary>
    /// Init Simulator with CUE
    /// </summary>
    void Init(SimulationController simulationController, float compartmentVolume);

    /// <summary>
    /// Reload data from CUE
    /// </summary>
    void Reload(SimulationController simulationController, float compartmentVolume);

    /// <summary>
    /// Simulate a Step and return data
    /// </summary>
    /// <param name="stepDuration">Step duration in seconds.</param>
    SimulationStep Step(double stepDuration);
}