﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class MoleculeSpecies : ScriptableObject
{
    public string Name = "Molecule";
    public float Size;
    public float Mass;
    public int InitialQuantity;
    public int Id = -1;
    public string PdbName = "";

    public MoleculeSpecies(string name, float size, float mass, int initialQuantity, int id, string pdbName)
    {
        Name = name;
        Size = size;
        Mass = mass;
        InitialQuantity = initialQuantity;
        Id = id;
        PdbName = pdbName;
    }

    public override string ToString()
    {
        return Name;
    }
}
