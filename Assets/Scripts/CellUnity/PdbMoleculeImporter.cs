﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System.Collections.Generic;
using System.IO;

public static class PdbMoleculeImporter
{
    /// <summary>
    /// Shows an OpenFilePanel and lets the user select a pdb file.
    /// </summary>
    public static void UserSelectFile()
    {

        string filename = EditorUtility.OpenFilePanel("PDB File", "", "pdb");

        if (string.IsNullOrEmpty(filename))
            return;

        AddMoleculeIngredient(filename);
    }

    /// <summary>
    /// Downloads a PDB file from a specified URL.
    /// Shows a Message Dialog when an error occurs.
    /// </summary>
    /// <returns><c>true</c>, if molecule was imported properly, <c>false</c> otherwise.</returns>
    /// <param name="url">URL.</param>
    /// <param name="name">Name.</param>
    public static bool DownloadFile(string url, string name)
    {
        /*WWW www = new WWW(url);

        while( !www.isDone ) {
            EditorUtility.DisplayProgressBar ("Download", "Downloading...", www.progress);
        }

        EditorUtility.ClearProgressBar ();

        if (!string.IsNullOrEmpty(www.error)) {

            EditorUtility.DisplayDialog("Error", www.error, "Close");
            return false;

        } else {

            PdbParser p = PdbParser.FromString(url, www.text, name);
            Molecule m = p.Parse();
            Create(m);

            return true;
        }*/

        return false;   // TODO delete
    }

    public static void AddMoleculeIngredient(string pdbName)
    {
        // Create a new species and add it to the CUE
        MoleculeSpecies species = ParseFile(pdbName);
        CUE.Instance.AddSpecies(species);
    }

    public static void ReloadMoleculeIngredient(MoleculeSpecies s)
    {
        MoleculeSpecies species = ParseFile(s.PdbName);
        s.Size = species.Size;  // If file has been changed.
        s.Mass = species.Mass;  // If file has been changed.
        s.Id = species.Id;  // Has to be updated.
    }

    private static MoleculeSpecies ParseFile(string pdbName)
    {
        // Load atom set from pdb file
        var atomSet = PdbLoader.ReadAtomData(pdbName);

        // If the set is empty return
        if (atomSet.Count == 0) return null;

        var atomSpheres = AtomHelper.GetAtomSpheres(atomSet);
        var centerPosition = AtomHelper.ComputeBounds(atomSpheres).center;

        var containsACarbonOnly = AtomHelper.ContainsACarbonOnly(atomSet);

        // Center atoms
        AtomHelper.OffsetSpheres(ref atomSpheres, centerPosition);

        // Compute bounds
        var bounds = AtomHelper.ComputeBounds(atomSpheres);

        // Define cluster decimation levels
        var clusterLevels = (containsACarbonOnly)
            ? new List<float>() { 0.85f, 0.25f, 0.1f }
            : new List<float>() { 0.2f, 0.1f, 0.05f };

        // Extract name from file path
        var name = Path.ChangeExtension(Path.GetFileName(pdbName), "").TrimEnd(new char[] { '.', ' ' });

        // Add ingredient type
        ComputeBufferManager.Instance.InitBuffers();    // ComputeBuffer.OnEnable is too slow 
        int id = SceneManager.Instance.AddIngredient(name, bounds, atomSpheres, MoleculeColorPalette.GetRandomColor(), clusterLevels, true);
        SceneManager.Instance.UploadProteinData();

        // Calculate mass of molecule
        float mass = 0;
        foreach (Atom atom in atomSet)
        {
            mass += atom.mass;
        }

        // Create a new species
        MoleculeSpecies m = ScriptableObject.CreateInstance<MoleculeSpecies>();
        m.Name = name;
        m.Size = Vector3.Magnitude(bounds.extents);
        m.Mass = mass;
        m.InitialQuantity = 0;
        m.Id = id;
        m.PdbName = pdbName;

        return m;
    }
}

public static class MoleculeColorPalette
{
    private static List<Color> pureColors = new List<Color>();
    private static List<Color> mixColors = new List<Color>();
    private static int _counter = 0;

    static MoleculeColorPalette()
    {
        // Add colors
        pureColors.Add(Color.red);
        pureColors.Add(Color.green);
        pureColors.Add(Color.blue);
        pureColors.Add(Color.cyan);
        pureColors.Add(Color.yellow);
        pureColors.Add(Color.magenta);

        mixColors.Add(LightenColor(Color.red));
        mixColors.Add(LightenColor(Color.green));
        mixColors.Add(LightenColor(Color.blue));
        mixColors.Add(LightenColor(Color.cyan));
        mixColors.Add(LightenColor(Color.yellow));
        mixColors.Add(LightenColor(Color.magenta));

        mixColors.Add(DarkenColor(Color.red));
        mixColors.Add(DarkenColor(Color.green));
        mixColors.Add(DarkenColor(Color.blue));
        mixColors.Add(DarkenColor(Color.cyan));
        mixColors.Add(DarkenColor(Color.yellow));
        mixColors.Add(DarkenColor(Color.magenta));

        // Shuffle colors
        pureColors = ShuffleColors(pureColors);
        mixColors = ShuffleColors(mixColors);
    }

    public static Color LightenColor(Color c)
    {
        Color w = Color.white;
        return new Color((c.r + w.r) / 2, (c.g + w.g) / 2, (c.b + w.b) / 2);
    }

    public static Color DarkenColor(Color c)
    {
        Color b = Color.black;
        return new Color((c.r + b.r) / 2, (c.g + b.g) / 2, (c.b + b.b) / 2);
    }

    public static Color GetRandomColor()
    {
        Color c;
        if (_counter < 6) c = pureColors[_counter % pureColors.Count];
        else c = mixColors[_counter % mixColors.Count];
        _counter++;

        return c;
    }

    private static List<Color> ShuffleColors(List<Color> c)
    {
        int n = c.Count;
        while (n > 1)
        {
            n--;
            int k = Random.Range(0, n + 1);
            Color temp = c[k];
            c[k] = c[n];
            c[n] = temp;
        }

        return c;
    }
}
