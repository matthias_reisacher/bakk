﻿using UnityEngine;
using System.Collections;
using System.Text;

[System.Serializable]
public class ReactionType : ScriptableObject
{
    public string Name = "";
    public float Rate = 1;
    public bool NeedsProtein = false; 
    public MoleculeSpecies[] Reagents = new MoleculeSpecies[] { };
    public MoleculeSpecies[] Products = new MoleculeSpecies[] { };

    public int[] GetIdsAsArray()
    {
        int[] ids = new int[Reagents.Length + Products.Length];
        int i = 0;

        foreach(MoleculeSpecies reagent in Reagents)
        {
            ids[i] = reagent.Id;
            i++;
        }

        foreach(MoleculeSpecies product in Products)
        {
            ids[i] = product.Id;
            i++;
        }

        return ids;
    }

    private string SpeciesToString(MoleculeSpecies species)
    {
        if (species == null) { return "?"; }
        return species.Name;
    }

    public override string ToString()
    {
        if(Reagents.Length == 0)
        {
            return "empty";
        }

        StringBuilder s = new StringBuilder();

        bool addPlus = false;
        foreach (MoleculeSpecies reagent in Reagents)
        {
            if (addPlus) { s.Append(" + "); }
            s.Append(SpeciesToString(reagent));

            addPlus = true;
        }

        s.Append(" \u2192 "); // arrow

        addPlus = false;
        foreach (MoleculeSpecies product in Products)
        {
            if (addPlus) { s.Append(" + "); }
            s.Append(SpeciesToString(product));

            addPlus = true;
        }

        if (string.IsNullOrEmpty(Name))
        {
            return s.ToString();
        }
        else
        {
            return Name + " (" + s.ToString() + ")";
        }

    }

    public string GetAutoName()
    {
        return Name + "reaction" + Mathf.Abs(this.GetInstanceID()).ToString();
    }
}
