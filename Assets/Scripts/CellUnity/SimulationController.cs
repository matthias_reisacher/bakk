﻿using UnityEngine;
using System;

public class SimulationController
{
    public static int CompartmentBuffersSizeMax = 100000;
    public static int NumCompartmentCells = 10000;

    public static int NumPerformReactionsMax = 100;
    public static int MaxCollisions = 200;
    public static int MaxNewReactions = 20;
    public static int MaxNewReactionSpecies = MaxNewReactions * 10;
    public static int MaxNewReactionRadii = MaxNewReactions * 5;

    public static int _cellsPerAxis = 10;  // Number of Cells on 1 axis
    private int _cellWidth; // Width of 1 cell
    private Vector4 _compartmentPosition;
    public int CompartmentRadius;

    private int _numSpecies = 0;
    private int _firstSpeciesId;
    // Counter for compartment data
    // [0]: number of molecules in the compartment (= length of _molecules)
    // [1]: number of molecules in the scene (= length of MoleculeInstanceInfos/-Positions/-Rotations)
    // [2]: assumed number of molecules in the scene (= length of MoleculeInstanceInfos/-Positions/-Rotations)
    // [_counterOffset] - [_numSpecies + _counterOffset]: free molecules of the specific species
    // [2] is only needed on the GPU to save the assumed number of molecules. The CPU manipulates directly [1] with the assumed values. The counters are corrected/adjusted with CorrectCounter(). 
    private int[] _counter = null;
    private int _counterOffset = 3;
    private float _deltaTimeLastCompartmentCheck = 0;

    public static float ReactionSpeed = 120.0f;
    public static int EnableFastCollisionDetection = 1;
    public static float CompartmentCheckInterval = 5.0f;

    // Data for creating new reactions
    private int _indexContent = 0;
    private int _indexSpecies = 0;
    private int _indexRadii = 0;
    private int[] _content;
    private int[] _species;
    private float[] _radii;
    // Reactions with a larger number of products than reagents are critical reactions, because the exceeding products may are appended to the molecule-buffer.
    // (The reaction's reagents are overwritten by the proteins. Additionally, deleted molecuels are also overwritten to minimize buffer length).
    // Therefore, the number of molecules in the compartment (=_coutner[0]) have to be increased by one for every critical reaction during createReaction.
    // The assumed number of molecules is corrected by calling the method FillCounter(), which is done every CompartmentCheckInterval-seconds.
    private int _numberOfNewCriticalReactions = 0;

    // Queues reactions that could not be executed when initiated.
    private ShortKeyDict<ReactionType, int> openReactions;

    private bool visualizationEnabled = false;

    private System.Random randomGenerator = new System.Random();

    //*****/

    // SimulationController buffers
    private ComputeBuffer _molecules;   // Contains all molecules inside the compartment
    private ComputeBuffer _proteins;    // Contains all proteins inside the compartment
    private ComputeBuffer _reactions;    // Contains all reactions
    private ComputeBuffer _reagents;    // Contains the chosen reagents of a reaction
    private ComputeBuffer _products;    // Contains the resulting products of a reaction
    private ComputeBuffer _transferedData; // Data which has to be transfered back to the CPU
    private ComputeBuffer _indices; // Data which has to be transfered back to the CPU
    private ComputeBuffer _collisions;    // Contains every collision which has to be solved
    private ComputeBuffer _performReactions;    // Contains every reaction which is ready to be performed
    private ComputeBuffer _binCounter;  // Contains the number of molecules for each cell
    private ComputeBuffer _prefixSum;   // Contains the prefix sum for counting sort
    private ComputeBuffer _cells; // CompartmentMolecules ordered by cell-index
    private ComputeBuffer _freeIndices; // Contains the indices of all deleted Instances; int2(_molecules, MoleculeInstanceInfo/-Position/-Rotation)

    //*****//

    #region Simulation_functions

    public void UpdateSimulation()
    {
        if (_counter[0] == 0) return;
        
        ResetTempBuffers();
        
        // Update time
        _deltaTimeLastCompartmentCheck += Time.deltaTime;
        if (_deltaTimeLastCompartmentCheck >= CompartmentCheckInterval)
        {
            // Run 'CorrectCounter' BEFORE 'PrepareMolecules' to prevent the usage of the assumed values!
            CorrectCounter();
            FillCounter();
            
            // Set (correct) number of molecule instances
            SceneManager.Instance.NumMoleculeInstances = _counter[1];

            // Check for molecules which moved inside the compartment during the simulation.
            // Has to be disabled because the inner-counter of the _molecule AppendBuffer is not updated correctly.
            // Check auf the shader function 'PerformReactions2' for further information.
            //PrepareMolecules(false);
            //FillCounter();
            _deltaTimeLastCompartmentCheck = 0;
        }

        BinCounter();
        PrefixSum();
        OrderCells();
        CalculateMovementVectors();
        MoveCompartmentMolecules();
        SolveCollisions();
        PerformReactions();

        if (_indexContent > 0) CreateNewReactions();
            TryInitiateOpenReactions();

        if(visualizationEnabled)
        {
            UpdateVisualizationLines(); // Must be run before CreateVisualizationLines!
            CreateVisualizationLines();
        }
    }

    public void Initialize()
    {
        InitBuffers();
    }

    public void Release()
    {
        ReleaseBuffers();
    }

    public bool InitiateReaction(ReactionType reaction, bool queueIfNotPossible)
    {
        //Debug.Log("Initiating Reaction....");
        int numReagents = reaction.Reagents.Length;
        bool moleculesFound = true;

        // Check if enough free reagents are in the compartment
        for (int i = 0; i < numReagents; i++)
        {
            int speciesId = reaction.Reagents[i].Id;
            if (GetSpeciesQuantity(speciesId) <= 0)
            {
                moleculesFound = false;
            }
        }

        // Perform reaction if enough molecules are available.
        if (moleculesFound)
        {
            // Return true if the new reaction could be created.
            if (AddReaction(reaction)) return true;
        }
        
        if(queueIfNotPossible)
        {
            ShortKeyDict<ReactionType, int>.Entry entry;
            if (!openReactions.Find(reaction, out entry))
            {
                entry = openReactions.Set(reaction, 0);
            }

            entry.Value++;
        }

        return false;
    }

    public void CreateNewReactions()
    {
        // Create and fill TempBuffers
        ComputeBuffer TempContent = new ComputeBuffer(_indexContent, 4);
        TempContent.SetData(_content);
        ComputeBuffer TempSpecies = new ComputeBuffer(_indexSpecies, 4);
        TempSpecies.SetData(_species);
        ComputeBuffer TempProductRadii = new ComputeBuffer(_indexRadii, 4);
        TempProductRadii.SetData(_radii);

        CreateReactions(TempContent, TempSpecies, TempProductRadii, _indexContent, false);

        // Release TempBuffers
        TempContent.Release();
        TempSpecies.Release();
        TempProductRadii.Release();

        // Add the number of critical reactions to the counter
        IncreaseCounterByCriticalReactions();

        // Set (assumed) new number of molecule instances
        SceneManager.Instance.NumMoleculeInstances = _counter[1];

        // Empty ReactionArrays
        ResetNewReactionsArrays();
    }

    public void InitiateVisualization(ReactionType reaction)
    {
        // Throw exception if visualization cannot be initialized
        if (!CheckVisualization(reaction))
            throw new System.Exception("Currently are not enough free reagents inside of the compartment. Visualization stopped!");

        int numReagents = reaction.Reagents.Length;
        int numProducts = reaction.Products.Length;

        // Create Temp Arrays
        int[] content = new int[MaxNewReactions * 3];
        int[] species = new int[MaxNewReactionSpecies];
        float[] radii = new float[MaxNewReactionRadii];

        // Set data
        content[0] = numReagents;
        content[1] = numProducts;
        if (reaction.NeedsProtein) content[2] = 1;
        else content[2] = 0;

        var speciesIds = reaction.GetIdsAsArray();
        Array.Copy(speciesIds, 0, species, 0, speciesIds.Length);

        var boundingSpheres = SceneManager.Instance.ProteinBoundingSpheres;
        for (int i = 0; i < numProducts; i++)
            radii[i] = boundingSpheres[speciesIds[numReagents + i]];

        // Increase indices
        int contentLength = 3;
        int speciesLength = numReagents + numProducts;
        int radiiLength = numProducts;

        // Check for critical reaction
        int numCriticalReactions = 0;
        if (numProducts > numReagents) _numberOfNewCriticalReactions += numProducts - numReagents;

        StartVisualization(content, species, radii, contentLength, speciesLength, radiiLength, numCriticalReactions);
    }

    public void StartVisualization(int[] content, int[]species, float[]radii, int contentLength, int speciesLength, int radiiLength, int numCriticalReactions)
    {
        // Create and fill TempBuffers
        ComputeBuffer TempContent = new ComputeBuffer(contentLength, 4);
        TempContent.SetData(content);
        ComputeBuffer TempSpecies = new ComputeBuffer(speciesLength, 4);
        TempSpecies.SetData(species);
        ComputeBuffer TempProductRadii = new ComputeBuffer(radiiLength, 4);
        TempProductRadii.SetData(radii);
        
        CreateReactions(TempContent, TempSpecies, TempProductRadii, contentLength, true);

        // Release TempBuffers
        TempContent.Release();
        TempSpecies.Release();
        TempProductRadii.Release();

        // Add the number of critical reactions to the counter
        for (int i = 0; i < _counterOffset; i++)
            _counter[i] += numCriticalReactions;

        // Set (assumed) new number of molecule instances
        SceneManager.Instance.NumMoleculeInstances = _counter[1];

        // Empty ReactionArrays
        ResetNewReactionsArrays();
    }

    #endregion

    #region Shader_functions

    private void PrepareCompartment()
    {
        PrepareMolecules(true);
        PrepareProteins();
        CorrectCounter();
        FillCounter();

        Debug.Log("Simulation started with " + _counter[0] + " reactants.");
    }

    private void PrepareMolecules(bool firstRun)
    {
        // During the first execution of prepare molecules, the overall number of molcules have to be counted as well as the buffer-location of deleted molecules.
        // Those line have to be ignored during later executions.
        int enableFirstRun = 0;
        if (firstRun) enableFirstRun = 1;

        int kernel = ComputeShaderManager.Instance.SimulatorPrepareCleanup.FindKernel("PrepareMolecules");
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetBuffer(kernel, "_MoleculeInstanceInfo", ComputeBufferManager.Instance.MoleculeInstanceInfos);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetBuffer(kernel, "_MoleculeInstancePositions", ComputeBufferManager.Instance.MoleculeInstancePositions);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetBuffer(kernel, "_MoleculesAppend", _molecules);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetBuffer(kernel, "_MoleculeAdditionalInfos", ComputeBufferManager.Instance.MoleculeAdditionalInfos);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetBuffer(kernel, "_TransferedData", _transferedData);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetBuffer(kernel, "_AtomStart", ComputeBufferManager.Instance.ProteinAtomStart);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetBuffer(kernel, "_AtomCount", ComputeBufferManager.Instance.ProteinAtomCount);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetBuffer(kernel, "_FreeIndices", _freeIndices);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetBuffer(kernel, "_Indices", _indices);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetBuffer(kernel, "_CullFlags", ComputeBufferManager.Instance.MoleculeInstanceCullFlags);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetInt("_FirstSpeciesId", _firstSpeciesId);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetInt("_CounterOffset", _counterOffset);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetInt("_FirstRun", enableFirstRun);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetVector("_CompartmentPosition", _compartmentPosition);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetInt("_CompartmentRadius", CompartmentRadius);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.Dispatch(kernel, SceneManager.Instance.NumMoleculeInstances, 1, 1);
    }

    private void PrepareProteins()
    {
        int kernel = ComputeShaderManager.Instance.SimulatorPrepareCleanup.FindKernel("PrepareProteins");
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetBuffer(kernel, "_ProteinInfo", ComputeBufferManager.Instance.ProteinInstanceInfos);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetBuffer(kernel, "_ProteinPositions", ComputeBufferManager.Instance.ProteinInstancePositions);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetBuffer(kernel, "_AtomCount", ComputeBufferManager.Instance.ProteinAtomCount);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetBuffer(kernel, "_AtomStart", ComputeBufferManager.Instance.ProteinAtomStart);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetBuffer(kernel, "_Proteins", _proteins);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetBuffer(kernel, "_Indices", _indices);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetBuffer(kernel, "_CullFlags", ComputeBufferManager.Instance.ProteinInstanceCullFlags);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetVector("_CompartmentPosition", _compartmentPosition);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetInt("_CompartmentRadius", CompartmentRadius);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.Dispatch(kernel, SceneManager.Instance.NumProteinInstances, 1, 1);
    }

    private void CleanUp()
    {
        // Fill _counter with the real values from the GPU and set the correct amount of molecules in the scene
        FillCounter();
        SceneManager.Instance.NumMoleculeInstances = _counter[1];

        int kernel = ComputeShaderManager.Instance.SimulatorPrepareCleanup.FindKernel("CleanUp");
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetBuffer(kernel, "_Molecules", _molecules);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.SetBuffer(kernel, "_MoleculeInstanceInfo", ComputeBufferManager.Instance.MoleculeInstanceInfos);
        ComputeShaderManager.Instance.SimulatorPrepareCleanup.Dispatch(kernel, _counter[0], 1, 1);
    }

    private void CreateReactions(ComputeBuffer content, ComputeBuffer species, ComputeBuffer productRadii, int contentSize, bool createVisLines)
    {
        int enableVisLines = 0;
        if (createVisLines) enableVisLines = 1;
        
        int kernel = ComputeShaderManager.Instance.Simulator.FindKernel("CreateReaction");
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_MoleculeInstanceInfos", ComputeBufferManager.Instance.MoleculeInstanceInfos);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_MoleculeInstancePositions", ComputeBufferManager.Instance.MoleculeInstancePositions);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_Indices", _indices);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_MoleculesRW", _molecules);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_TransferedData", _transferedData);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_ReactionsAppend", _reactions);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_ReagentsAppend", _reagents);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_ProductsAppend", _products);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_Content", content);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_SpeciesId", species);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_ProductRadii", productRadii);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_Proteins", _proteins);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_ProteinPositions", ComputeBufferManager.Instance.ProteinDisplayPositions);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_ProteinRotations", ComputeBufferManager.Instance.ProteinDisplayRotations);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_Atoms", ComputeBufferManager.Instance.ProteinAtoms);
        ComputeShaderManager.Instance.Simulator.SetInt("_FirstSpeciesId", _firstSpeciesId);
        ComputeShaderManager.Instance.Simulator.SetInt("_CounterOffset", _counterOffset);
        ComputeShaderManager.Instance.Simulator.SetInt("_ContentSize", contentSize);
        ComputeShaderManager.Instance.Simulator.SetInt("_Random", randomGenerator.Next(0, SceneManager.Instance.ProteinInstanceInfos.Count + 1));
        ComputeShaderManager.Instance.Simulator.SetInt("_CreateVisLines", enableVisLines);
        ComputeShaderManager.Instance.Simulator.Dispatch(kernel, 1, 1, 1);
    }

    private void CalculateMovementVectors()
    {
        int kernel = ComputeShaderManager.Instance.Simulator.FindKernel("ApplyRandomness");
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_MoleculeInstancePositions", ComputeBufferManager.Instance.MoleculeInstancePositions);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_MoleculesRW", _molecules);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_ReactionsRW", _reactions);
        ComputeShaderManager.Instance.Simulator.SetFloat("_Speed", ReactionSpeed * Time.deltaTime);
        ComputeShaderManager.Instance.Simulator.SetVector("_CompartmentPos", _compartmentPosition);
        ComputeShaderManager.Instance.Simulator.SetFloat("_CompartmentRadius", CompartmentRadius);
        ComputeShaderManager.Instance.Simulator.Dispatch(kernel, _counter[0], 1, 1);
    }

    private void MoveCompartmentMolecules()
    {
        int kernel = ComputeShaderManager.Instance.Simulator.FindKernel("MoveMolecules");
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_MoleculeInstancePositions", ComputeBufferManager.Instance.MoleculeInstancePositions);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_MoleculeInstanceRotations", ComputeBufferManager.Instance.MoleculeInstanceRotations);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_MoleculesRW", _molecules);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_CollisionsAppend", _collisions);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_Indices", _indices);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_BinCounter", _binCounter);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_PrefixSum", _prefixSum);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_OrderedIds", _cells);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_Proteins", _proteins);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_ProteinPositions", ComputeBufferManager.Instance.ProteinDisplayPositions);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_ProteinRotations", ComputeBufferManager.Instance.ProteinDisplayRotations);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_Atoms", ComputeBufferManager.Instance.ProteinAtoms);
        ComputeShaderManager.Instance.Simulator.SetInt("_NumCells", _cellsPerAxis);
        ComputeShaderManager.Instance.Simulator.SetFloat("_CellWidth", _cellWidth);
        ComputeShaderManager.Instance.Simulator.SetFloat("_Speed", ReactionSpeed * Time.deltaTime);
        ComputeShaderManager.Instance.Simulator.SetInt("_FastCD", EnableFastCollisionDetection);
        ComputeShaderManager.Instance.Simulator.Dispatch(kernel, _counter[0], 1, 1);
    }

    private void SolveCollisions()
    {
        int kernel = ComputeShaderManager.Instance.Simulator.FindKernel("SolveCollisions");
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_MoleculeInstancePositions", ComputeBufferManager.Instance.MoleculeInstancePositions);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_MoleculesRW", _molecules);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_ReactionsRW", _reactions);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_ReagentsRW", _reagents);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_PerformReactionsAppend", _performReactions);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_CollisionsRW", _collisions);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_Indices", _indices);
        ComputeShaderManager.Instance.Simulator.Dispatch(kernel, 1, 1, 1);
    }

    private void PerformReactions()
    {
        int kernel = ComputeShaderManager.Instance.Simulator.FindKernel("PerformReactions1");
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_MoleculeInstanceInfos", ComputeBufferManager.Instance.MoleculeInstanceInfos);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_MoleculesRW", _molecules);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_Reactions", _reactions);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_ReagentsRW", _reagents);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_PerformReactions", _performReactions);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_Indices", _indices);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_FreeIndicesRW", _freeIndices);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_MoleculeAdditionalInfos", ComputeBufferManager.Instance.MoleculeAdditionalInfos);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_ProteinAdditionalInfos", ComputeBufferManager.Instance.ProteinAdditionalInfos);
        ComputeShaderManager.Instance.Simulator.Dispatch(kernel, 1, 1, 1);

        // This kernel is neccessary to fix the append-buffer problem.
        // But for some reasons, the kernel is not working correctly.
        /*kernel = ComputeShaderManager.Instance.Simulator.FindKernel("PerformReactions2");
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_Reactions", _reactions);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_PerformReactions", _performReactions);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_Indices", _indices);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_MoleculesAppend", _molecules);
        ComputeShaderManager.Instance.Simulator.Dispatch(kernel, 1, 1, 1);*/

        kernel = ComputeShaderManager.Instance.Simulator.FindKernel("PerformReactions3");
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_MoleculeInstanceInfos", ComputeBufferManager.Instance.MoleculeInstanceInfos);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_MoleculeInstancePositions", ComputeBufferManager.Instance.MoleculeInstancePositions);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_MoleculesRW", _molecules);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_Reactions", _reactions);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_Products", _products);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_PerformReactions", _performReactions);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_Indices", _indices);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_FreeIndices", _freeIndices);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_MoleculeAdditionalInfos", ComputeBufferManager.Instance.MoleculeAdditionalInfos);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_TransferedData", _transferedData);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_AtomStart", ComputeBufferManager.Instance.ProteinAtomStart);
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_AtomCount", ComputeBufferManager.Instance.ProteinAtomCount);
        ComputeShaderManager.Instance.Simulator.SetInt("_FirstSpeciesId", _firstSpeciesId);
        ComputeShaderManager.Instance.Simulator.SetInt("_CounterOffset", _counterOffset);
        ComputeShaderManager.Instance.Simulator.Dispatch(kernel, 1, 1, 1);
    }

    private void BinCounter()
    {
        int kernel = ComputeShaderManager.Instance.Countingsort.FindKernel("BinCounter");
        ComputeShaderManager.Instance.Countingsort.SetBuffer(kernel, "_MoleculeInstancePositions", ComputeBufferManager.Instance.MoleculeInstancePositions);
        ComputeShaderManager.Instance.Countingsort.SetBuffer(kernel, "_MoleculesRW", _molecules);
        ComputeShaderManager.Instance.Countingsort.SetBuffer(kernel, "_BinCounter", _binCounter);
        ComputeShaderManager.Instance.Countingsort.SetVector("_CPos", _compartmentPosition);
        ComputeShaderManager.Instance.Countingsort.SetFloat("_CRadius", CompartmentRadius);
        ComputeShaderManager.Instance.Countingsort.SetInt("_NumCells", _cellsPerAxis);
        ComputeShaderManager.Instance.Countingsort.SetFloat("_CellWidth", _cellWidth);
        ComputeShaderManager.Instance.Countingsort.SetFloat("_CellWidthReciprocal", 1 / (float)_cellWidth);
        ComputeShaderManager.Instance.Countingsort.Dispatch(kernel, _counter[0], 1, 1);
    }

    private void PrefixSum()
    {
        int kernel = ComputeShaderManager.Instance.Countingsort.FindKernel("PrefixSum");
        ComputeShaderManager.Instance.Countingsort.SetBuffer(kernel, "_BinCounter", _binCounter);
        ComputeShaderManager.Instance.Countingsort.SetBuffer(kernel, "_PrefixSum", _prefixSum);
        ComputeShaderManager.Instance.Countingsort.SetInt("_NumCells", _cellsPerAxis);
        ComputeShaderManager.Instance.Countingsort.Dispatch(kernel, 1, 1, 1);
    }

    private void OrderCells()
    {
        int kernel = ComputeShaderManager.Instance.Countingsort.FindKernel("SortCMolecules");
        ComputeShaderManager.Instance.Countingsort.SetBuffer(kernel, "_MoleculesRW", _molecules);
        ComputeShaderManager.Instance.Countingsort.SetBuffer(kernel, "_Cells", _cells);
        ComputeShaderManager.Instance.Countingsort.SetBuffer(kernel, "_TransferedData", _transferedData);
        ComputeShaderManager.Instance.Countingsort.SetBuffer(kernel, "_PrefixSum", _prefixSum);
        ComputeShaderManager.Instance.Countingsort.Dispatch(kernel, 1, 1, 1);
    }

    private void CorrectCounter()
    {
        int kernel = ComputeShaderManager.Instance.Simulator.FindKernel("CorrectCounter");
        ComputeShaderManager.Instance.Simulator.SetBuffer(kernel, "_TransferedData", _transferedData);
        ComputeShaderManager.Instance.Simulator.Dispatch(kernel, 1, 1, 1);
    }

    private void CreateVisualizationLines()
    {
        int kernel = ComputeShaderManager.Instance.VisualizationLines.FindKernel("CreateVisLines");
        ComputeShaderManager.Instance.VisualizationLines.SetBuffer(kernel, "_MoleculeInstancePositions", ComputeBufferManager.Instance.MoleculeInstancePositions);
        ComputeShaderManager.Instance.VisualizationLines.SetBuffer(kernel, "_Molecules", _molecules);
        ComputeShaderManager.Instance.VisualizationLines.SetBuffer(kernel, "_Reactions", _reactions);
        ComputeShaderManager.Instance.VisualizationLines.SetBuffer(kernel, "_VisLinesAppend", ComputeBufferManager.Instance.VisLines);
        ComputeShaderManager.Instance.VisualizationLines.SetBuffer(kernel, "_Colors", ComputeBufferManager.Instance.ProteinColors);
        ComputeShaderManager.Instance.VisualizationLines.Dispatch(kernel, _counter[0], 1, 1);
    }

    private void UpdateVisualizationLines()
    {
        int kernel = ComputeShaderManager.Instance.VisualizationLines.FindKernel("UpdateVisLines");
        ComputeShaderManager.Instance.VisualizationLines.SetBuffer(kernel, "_MoleculeInstancePositions", ComputeBufferManager.Instance.MoleculeInstancePositions);
        ComputeShaderManager.Instance.VisualizationLines.SetBuffer(kernel, "_Molecules", _molecules);
        ComputeShaderManager.Instance.VisualizationLines.SetBuffer(kernel, "_VisLinesRW", ComputeBufferManager.Instance.VisLines);
        ComputeShaderManager.Instance.VisualizationLines.SetBuffer(kernel, "_Reactions", _reactions);
        ComputeShaderManager.Instance.VisualizationLines.Dispatch(kernel, ComputeBufferManager.NumVisLinesMax, 1, 1);
    }

    private void CleanupVisualizationLines()
    {
        int kernel = ComputeShaderManager.Instance.VisualizationLines.FindKernel("ClearVisLineFlags");
        ComputeShaderManager.Instance.VisualizationLines.SetBuffer(kernel, "_Molecules", _molecules);
        ComputeShaderManager.Instance.VisualizationLines.Dispatch(kernel, _counter[0], 1, 1);
    }

    #endregion

    #region Misc_functions

    private void FillCounter()
    {
        _counter = new int[_transferedData.count];
        _transferedData.GetData(_counter);
    }

    private void IncreaseCounterByCriticalReactions()
    {
        for (int i = 0; i < _counterOffset; i++)
            _counter[i] += _numberOfNewCriticalReactions;
    }

    public void EnableSimulation(int compartmentRadius)
    {
        ResetBuffers();

        openReactions = new ShortKeyDict<ReactionType, int>();
        _numSpecies = SceneManager.Instance.NumMoleculeSpecies;
        _firstSpeciesId = SceneManager.Instance.FirstMoleculeId;
        CompartmentRadius = compartmentRadius;
        _cellWidth = Mathf.FloorToInt((CompartmentRadius * 2) / _cellsPerAxis);
        _compartmentPosition = SceneManager.Instance.GetSelectedElementPosition();
        _deltaTimeLastCompartmentCheck = 0;
        
        PrepareCompartment();
        ResetNewReactionsArrays();
    }

    public void DisableSimulation()
    {
        _numSpecies = 0;

        CleanUp();

        DisableVisualization();
    }

    public void EnableVisualization(ReactionType reaction)
    {
        visualizationEnabled = true;
        InitiateVisualization(reaction);
        CreateVisualizationLines();
    }

    public void DisableVisualization()
    {
        visualizationEnabled = false;
        CleanupVisualizationLines();
        ResetVisLines();
    }

    public int GetSpeciesQuantity(int speciesId)
    {
        if (_counter == null || _numSpecies == 0) return 0;

        int relativeId = speciesId - _firstSpeciesId;
        if (relativeId < 0 || relativeId >= _numSpecies) return 0;
        return _counter[relativeId + _counterOffset];
    }

    // Decreases the value of the specific species in _counter by 1.
    // Warning: This is only temporary - counter is filled by the GPU!
    public void IncreaseSpeciesQuantity(int speciesId, int value)
    {
        if (_counter == null || _numSpecies == 0) return;

        int relativeId = speciesId - _firstSpeciesId;
        if (relativeId < 0 || relativeId >= _numSpecies) return;

        _counter[speciesId - _firstSpeciesId + _counterOffset] += value;
    }

    public int GetCounterField(int index)
    {
        if (_counter == null) return 0;

        return _counter[index];
    }

    // Tries to initiate queued reactions.
    private void TryInitiateOpenReactions()
    {
        ShortKeyDict<ReactionType, int>.Entry entry = null;
        while (openReactions.GetNext(entry, out entry))
        {
            while (entry.Value > 0)
            {
                if (InitiateReaction(entry.Key, false))
                {
                    entry.Value--;
                    if (entry.Value == 0)
                    {
                        openReactions.Remove(entry.Key);
                        break;
                    }
                }
                else break;
            }
        }
    }

    private void ResetNewReactionsArrays()
    {
        _indexContent = 0;
        _indexSpecies = 0;
        _indexRadii = 0;
        _content = new int[MaxNewReactions * 3];
        _species = new int[MaxNewReactionSpecies];
        _radii = new float[MaxNewReactionRadii];
        _numberOfNewCriticalReactions = 0;
    }

    private bool AddReaction(ReactionType reaction)
    {
        int numReagents = reaction.Reagents.Length;
        int numProducts = reaction.Products.Length;

        // Check arrays
        if (_indexContent / 3 >= MaxNewReactions) return false;
        if (_indexSpecies + numReagents + numProducts >= MaxNewReactionSpecies) return false;
        if (_indexRadii + numProducts >= MaxNewReactionRadii) return false;

        // Set data
        _content[_indexContent] = numReagents;
        _content[_indexContent + 1] = numProducts;
        if (reaction.NeedsProtein) _content[_indexContent + 2] = 1;
        else _content[_indexContent + 2] = 0;

        var speciesIds = reaction.GetIdsAsArray();
        Array.Copy(speciesIds, 0, _species, _indexSpecies, speciesIds.Length);

        var boundingSpheres = SceneManager.Instance.ProteinBoundingSpheres;
        for (int i = 0; i < numProducts; i++)
        {
            _radii[_indexRadii + i] = boundingSpheres[speciesIds[numReagents + i]];
        }

        // Decrease counter values
        for (int i = _indexSpecies; i < _indexSpecies + numReagents; i++)
        {
            IncreaseSpeciesQuantity(_species[i], -1);
        }

        // Increase indices
        _indexContent += 3;
        _indexSpecies += numReagents + numProducts;
        _indexRadii += numProducts;

        // Check for critical reaction
        if (numProducts > numReagents) _numberOfNewCriticalReactions += numProducts - numReagents;

        return true;
    }

    // Checks if the specified reaction is already queued or if enough free reagents are available.
    public bool CheckVisualization(ReactionType reaction)
    {
        // Check Queue
        ShortKeyDict<ReactionType, int>.Entry entry;
        if (openReactions.Find(reaction, out entry))
        {
            entry.Value--;
            return true;
        }
        
        // Check if enough free reagents are in the compartment
        bool moleculesFound = true;
        for (int i = 0; i < reaction.Reagents.Length; i++)
        {
            int speciesId = reaction.Reagents[i].Id;
            if (GetSpeciesQuantity(speciesId) > 0)
                IncreaseSpeciesQuantity(speciesId, -1);
            else
            {
                // Reset decreased species quantity
                for (int j = i - 1; j >= 0; j--)
                    IncreaseSpeciesQuantity(reaction.Reagents[i].Id, 1);

                moleculesFound = false;
                break;
            }
        }

        return moleculesFound;
    }

    public void InitBuffers()
    {
        if (_reactions == null) _reactions = new ComputeBuffer(CompartmentBuffersSizeMax, 48, ComputeBufferType.Append);
        if (_reagents == null) _reagents = new ComputeBuffer(CompartmentBuffersSizeMax, 4, ComputeBufferType.Append);
        if (_products == null) _products = new ComputeBuffer(CompartmentBuffersSizeMax, 8, ComputeBufferType.Append);
        if (_transferedData == null) _transferedData = new ComputeBuffer(SceneManager.Instance.NumMoleculeSpecies + _counterOffset, 4);
        if (_indices == null) _indices = new ComputeBuffer(1, sizeof(int) * 7);
        if (_collisions == null) _collisions = new ComputeBuffer(MaxCollisions, 48, ComputeBufferType.Append);
        if (_performReactions == null) _performReactions = new ComputeBuffer(NumPerformReactionsMax, 4, ComputeBufferType.Append);
        if (_binCounter == null) _binCounter = new ComputeBuffer(_cellsPerAxis * _cellsPerAxis * _cellsPerAxis, 4);
        if (_prefixSum == null) _prefixSum = new ComputeBuffer(_cellsPerAxis * _cellsPerAxis * _cellsPerAxis, 4);
        if (_cells == null) _cells = new ComputeBuffer(NumCompartmentCells, 4);
        if (_proteins == null) _proteins = new ComputeBuffer(CompartmentBuffersSizeMax, 16, ComputeBufferType.Append);
        if (_molecules == null) _molecules = new ComputeBuffer(CompartmentBuffersSizeMax, 80, ComputeBufferType.Append);
        if (_freeIndices == null) _freeIndices = new ComputeBuffer(CompartmentBuffersSizeMax, 8, ComputeBufferType.Append);

        ResetVisLines();
    }

    public void ResetBuffers()
    {
        if (_reactions != null) { ComputeBufferManager.ClearAppendBuffer(_reactions); _reactions.SetData(new int[CompartmentBuffersSizeMax * 12]); }
        if (_reagents != null) { ComputeBufferManager.ClearAppendBuffer(_reagents); _reagents.SetData(new int[CompartmentBuffersSizeMax]); }
        if (_products != null) { ComputeBufferManager.ClearAppendBuffer(_products); _products.SetData(new int[CompartmentBuffersSizeMax * 2]); }
        if (_transferedData != null) { _transferedData.SetData(new int[SceneManager.Instance.NumMoleculeSpecies + _counterOffset]); }
        if (_indices != null) { _indices.SetData(new int[7]); }
        if (_proteins != null) { ComputeBufferManager.ClearAppendBuffer(_proteins); _proteins.SetData(new int[CompartmentBuffersSizeMax * 16]); }
        if (_molecules != null) { ComputeBufferManager.ClearAppendBuffer(_molecules); _molecules.SetData(new int[CompartmentBuffersSizeMax * 20]); }
        if (_freeIndices != null) { ComputeBufferManager.ClearAppendBuffer(_freeIndices); _freeIndices.SetData(new int[CompartmentBuffersSizeMax]); }

        ResetTempBuffers();
    }

    public void ResetTempBuffers()
    {
        if (_collisions != null) { ComputeBufferManager.ClearAppendBuffer(_collisions); _collisions.SetData(new int[MaxCollisions * 12]); }
        if (_performReactions != null) { ComputeBufferManager.ClearAppendBuffer(_performReactions); _performReactions.SetData(new int[NumPerformReactionsMax]); }
        if (_binCounter != null) { _binCounter.SetData(new int[_cellsPerAxis * _cellsPerAxis * _cellsPerAxis]); }
        if (_prefixSum != null) { _prefixSum.SetData(new int[_cellsPerAxis * _cellsPerAxis * _cellsPerAxis]); }
        if (_cells != null) { _cells.SetData(new int[NumCompartmentCells]); }
    }

    public void ResetVisLines()
    {
        if (ComputeBufferManager.Instance.VisLines != null)
        {
            ComputeBufferManager.ClearAppendBuffer(ComputeBufferManager.Instance.VisLines);
            ComputeBufferManager.Instance.VisLines.SetData(new int[ComputeBufferManager.NumVisLinesMax * 12]);
        }
    }

    public void ReleaseBuffers()
    {
        if (_reactions != null) { _reactions.Release(); _reactions = null; }
        if (_reagents != null) { _reagents.Release(); _reagents = null; }
        if (_products != null) { _products.Release(); _products = null; }
        if (_transferedData != null) { _transferedData.Release(); _transferedData = null; }
        if (_indices != null) { _indices.Release(); _indices = null; }
        if (_collisions != null) { _collisions.Release(); _collisions = null; }
        if (_performReactions != null) { _performReactions.Release(); _performReactions = null; }
        if (_binCounter != null) { _binCounter.Release(); _binCounter = null; }
        if (_prefixSum != null) { _prefixSum.Release(); _prefixSum = null; }
        if (_cells != null) { _cells.Release(); _cells = null; }
        if (_proteins != null) { _proteins.Release(); _proteins = null; }
        if (_molecules != null) { _molecules.Release(); _molecules = null; }
        if (_freeIndices != null) { _freeIndices.Release(); _freeIndices = null; }
    }

    #endregion
}
