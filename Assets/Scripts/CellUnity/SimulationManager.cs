﻿using UnityEngine;
using System;
using System.Threading;
using System.Collections.Generic;


// Simulation state.
public enum SimulationState
{
    Stopped, Paused, Running
}


public class SimulationManager : MonoBehaviour {

    private SimulationController _controller;
    private VisualizationGUI _visualization;
    private static bool _pauseSimulationController = false;
    public static bool IsPaused { get { return _pauseSimulationController; } }
    private static bool _isSimulating = false;
    public static bool IsSimulating { get { return _isSimulating; } }
    public static float Speed
    {
        get { return SimulationController.ReactionSpeed; }
        set { SimulationController.ReactionSpeed = value; }
    }
    public static bool EnableFastCollisionDetection
    {
        get
        {
            if (SimulationController.EnableFastCollisionDetection == 1) return true;
            else return false;
        }
        set
        {
            if (value) SimulationController.EnableFastCollisionDetection = 1;
            else SimulationController.EnableFastCollisionDetection = 0;
        }
    }
    public static float CompartmentCheckInterval {
        get { return SimulationController.CompartmentCheckInterval; }
        set { SimulationController.CompartmentCheckInterval = value; }
    }
    public static bool EnableCopasiOutput = false;
    public static Color FixedVisLineColor = Color.red;
    public static int UseFixedVisLineColor = 0;

    //*****/

    private ISimulator simulator;   // Simulator used by CellUnity
    private SimulationState state = SimulationState.Stopped;  // Current state of the simulator

    // Can only be set in constructor.
    // If true, the simulation is run in the main thread,
    // if false, a new thread is started for the simulation.
    private readonly bool runInMainThread = true;
    private Thread simulationThread;    // Simulation thread.

    public SimulationState State { get { return state; } }  // Gets the current state of the simulator.

    DateTime nextStep;  // Time that indicates when the next simulation step is simulated.
    private Queue<SimulationStep> stepsQueue = new Queue<SimulationStep>(); // Queue which holds simulation results
    private readonly object stepsQueueLock = new object();  // Thread lock object of the stepsQueue.

    //*****/

    // Declare the reaction manager as a singleton
    private static SimulationManager _instance = null;
    public static SimulationManager Instance
    {
        get
        {
            if (_instance != null) return _instance;

            _instance = FindObjectOfType<SimulationManager>();
            if (_instance == null)
            {
                var go = GameObject.Find("_SimulationManager");
                if (go != null) DestroyImmediate(go);

                go = new GameObject("_SimulationManager") { hideFlags = HideFlags.HideInInspector };
                _instance = go.AddComponent<SimulationManager>();
            }
            
            return _instance;
        }
    }

    void OnEnable()
    {
        _controller = new SimulationController();
        _isSimulating = false;
        _controller.Initialize();

        _visualization = GameObject.Find("_SimulationManager").AddComponent<VisualizationGUI>();
        _visualization.enabled = false;
        _pauseSimulationController = false;
    }

    void OnDisable()
    {
        _isSimulating = false;
        StopSimulation();
        _controller.Release();

        _visualization.enabled = false;
    }

    //*****/

    void Update()
    {
        if (!_isSimulating) return;
        

        if (runInMainThread)
        {
            MainThreadRunSimulation();
        }

        // Process Simulation Results
        while (true)
        {
            SimulationStep step;
            lock (stepsQueueLock)
            {
                if (stepsQueue.Count == 0)  break;
                step = stepsQueue.Dequeue();
            }

            System.Text.StringBuilder info = new System.Text.StringBuilder();

            foreach (ReactionCount item in step.Reactions)
            {
                if(item.Count > 0 && EnableCopasiOutput)   info.Append(item.ReactionType.ToString() + ": " + item.Count + ".\n");

                for (ulong i = 0; i < item.Count; i++)
                {
                    _controller.InitiateReaction(item.ReactionType, true);
                }
            }
            
            // Debug output
            if(info.Length > 0 && EnableCopasiOutput) Debug.Log(info.ToString());
        }
        
        // Update SimulationController
        if (!_pauseSimulationController)
            _controller.UpdateSimulation();
    }

    public void EnableSimulation()
    {
        if (!Application.isPlaying) throw new System.Exception("Can only simulate in Play Mode");
        if (SceneManager.Instance.SelectedElement == -1) throw new System.Exception("Select a protein before starting the simulation.");
        if (!PersistantSettings.Instance.EnableBrownianMotion) Debug.Log("Warning: It is highly recommended that brownian motion is enabled during the simulation.");

        _isSimulating = true;
        _controller.EnableSimulation(CUE.Instance.Radius);
        
        ReloadSimulation();
        StartSimulation();

        _visualization.enabled = true;
        _pauseSimulationController = false;
    }

    public void DisableSimulation()
    {
        _isSimulating = false;
        _controller.DisableSimulation();

        StopSimulation();

        _visualization.enabled = false;
    }

    public int GetNumInside()
    {
        return _controller.GetCounterField(0);
    }

    public int GetNumOutside()
    {
        return _controller.GetCounterField(1);
    }

    public void ReloadSimulation()
    {
        SimulationState oldState = state;
        if (state != SimulationState.Stopped)
        {
            StopSimulation();
        }

        if (simulator == null)
        {
            this.simulator = new CopasiSimulator();
            simulator.Init(_controller, CUE.Instance.Volume);
        }
        else
        {
            simulator.Reload(_controller, CUE.Instance.Volume);
        }

        simulationThread = new Thread(new ThreadStart(RunSimulation));
        simulationThread.IsBackground = true;

        // restore old state
        if (oldState == SimulationState.Running)        StartSimulation();
        else if (oldState == SimulationState.Paused)    PauseSimulation();
        else if (oldState == SimulationState.Stopped)   state = SimulationState.Stopped;
    }
    
    // Starts the simulation. Can only be called in Play Mode
    public void StartSimulation()
    {
        if (!Application.isPlaying)
        {
            throw new System.Exception("can only simulate in Play Mode");
        }

        if (state == SimulationState.Stopped)
        {
            ReloadSimulation();

            state = SimulationState.Running;

            nextStep = DateTime.Now.AddSeconds(0.5);

            if (!runInMainThread)
            {
                simulationThread.Start();
            }

            Debug.Log("Simulation start");
        }
        else if (state == SimulationState.Paused)
        {
            state = SimulationState.Running;
        }
    }

    // Pauses the Simulation.
    public void PauseSimulation()
    {
        if (state == SimulationState.Stopped)
        {
            StartSimulation();
        }

        if (state == SimulationState.Running)
        {
            state = SimulationState.Paused;
            Debug.Log("Simulation pause");
        }
    }

    // Stops the Simulation.
    public void StopSimulation()
    {
        if ((state == SimulationState.Running || state == SimulationState.Paused) && simulationThread != null)
        {
            if (!runInMainThread)
            {
                simulationThread.Abort();
                simulationThread.Join();
            }
            simulationThread = null;

            state = SimulationState.Stopped;

            Debug.Log("Simulation stop");
        }
    }

    // Simulates one step.
    private void SimulateStep()
    {
        //Debug.Log("Step");  // TODO delete

        SimulationStep step = simulator.Step(CUE.Instance.SimulationStep);
        EnqueueStep(step);
        nextStep = nextStep.AddSeconds(CUE.Instance.VisualizationStep);
    }

    // Can be called every frame. Runs a Simulation Step if necessary.
    public void MainThreadRunSimulation()
    {
        if (state == SimulationState.Paused)
        {
            nextStep = DateTime.Now.AddSeconds(0.5);
        }
        else if (state == SimulationState.Stopped)
        {
            return;
        }
        else if (state == SimulationState.Running)
        {
            if (nextStep <= DateTime.Now)
            {
                SimulateStep();
            }
        }
    }
    
    // Simulation Thread Method.
    private void RunSimulation()
    {
        try
        {
            while (true)
            {
                // Free CPU by call Thread.Sleep
                while (nextStep > DateTime.Now)
                {
                    int sleepTime = System.Math.Max(0, (int)((nextStep - DateTime.Now).TotalMilliseconds * 0.95));
                    Thread.Sleep(sleepTime);
                }

                // Wait while Simulation is paused
                while (state == SimulationState.Paused)
                {
                    Thread.Sleep(500);
                    nextStep = DateTime.Now;
                }

                // Simulate Step
                SimulateStep();
            }
        }
        catch (ThreadAbortException)
        {
            Debug.Log("Thread abort");
            // is never executed, I don't know why
            // doesn't Unity support threading?
        }
        finally
        {
            // is never executed, I don't know why
            // doesn't Unity support threading?
        }
    }

    // Enqueue a Simulation Result. Thread-safe.
    private void EnqueueStep(SimulationStep step)
    {
        lock (stepsQueueLock)
        {
            stepsQueue.Enqueue(step);
        }
    }

    public void ToggleSimulationController()
    {
        _pauseSimulationController = !_pauseSimulationController;
    }

    public void InitVisualization(ReactionType reaction)
    {
        _controller.EnableVisualization(reaction);
        _pauseSimulationController = true;
    }

    public void RunVisualization()
    {
        _pauseSimulationController = false;
    }

    public void StopVisualization()
    {
        _controller.DisableVisualization();
    }
}