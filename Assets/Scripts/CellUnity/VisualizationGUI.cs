﻿using UnityEditor;
using UnityEngine;
using UnityEngine.UI;

enum ButtonState
{
    Init = 0,
    Start = 1,
    Stop = 2
};

public class VisualizationGUI : MonoBehaviour
{
    private ReactionType[] reactions;
    private string[] reactionStrings;
    private string[] colorStrings = new string[] { "Ingredient-Color", "Fixed-Color" };
    private int selection = 0;
    private bool enableVis = true;
    private ButtonState button = ButtonState.Init;

    private int guiBoxY;
    private const int guiBoxSpace = 10;
    private const int guiBoxBorder = 7;
    private const int guiBoxHeader = 24;
    private const int guiBoxWidth = 250;

    void OnEnable()
    {
        // Fill dropdown list with the reaction's names.
        reactions = CUE.Instance.ReactionTypes;
        reactionStrings = new string[reactions.Length];

        for (int i = 0; i < reactions.Length; i++)
            reactionStrings[i] = reactions[i].ToString();

        if (reactionStrings.Length == 0)
            enableVis = false;
        else
            enableVis = true;

        button = ButtonState.Init;
    }

    void OnGUI()
    {
        GuiBoxReset();
        GuiBeginBox("Reaction-Visualization", 170 + 22 * reactionStrings.Length);

        GUILayout.Label("Reactions: ");

        selection = GUILayout.SelectionGrid(selection, reactionStrings, 1, "toggle");

        GUILayout.Space(10);

        GUILayout.Label("Linecolor:");

        SimulationManager.UseFixedVisLineColor = GUILayout.SelectionGrid(SimulationManager.UseFixedVisLineColor, colorStrings, 1, "toggle");

        GUILayout.Space(10);
       
        GUI.enabled = enableVis;
        switch (button)
        {
            case ButtonState.Init:
                if (GUILayout.Button("Initialize"))
                {
                    SimulationManager.Instance.InitVisualization(reactions[selection]);
                    button = ButtonState.Start;
                }
                break;
            case ButtonState.Start:
                if (GUILayout.Button("Start"))
                {
                    SimulationManager.Instance.RunVisualization();
                    button = ButtonState.Stop;
                }
                break;
            case ButtonState.Stop:
                if (GUILayout.Button("Stop"))
                {
                    SimulationManager.Instance.StopVisualization();
                    button = ButtonState.Init;
                }
                break;
            default:
                break;
        }


        GUI.enabled = (button == ButtonState.Start) ? false : true;
        var text = "Pause";
        if (SimulationManager.IsPaused) text = "Play";
        if (GUILayout.Button(text))
        {
            SimulationManager.Instance.ToggleSimulationController();
        }

        GuiEndBox();
    }

    private void GuiBoxReset()
    {
        guiBoxY = 0;
    }

    private void GuiBeginBox(string text, int h)
    {
        guiBoxY += guiBoxSpace;

        int totalH = h + 2 * guiBoxBorder + guiBoxHeader;

        Rect boxRect = new Rect(guiBoxSpace, guiBoxY, guiBoxWidth, totalH);
        GUI.Box(boxRect, "");

        GUI.Box(new Rect(boxRect.x, boxRect.y, boxRect.width, guiBoxHeader), text);

        Rect areaRect = new Rect(boxRect.x + guiBoxBorder, guiBoxY + guiBoxBorder + guiBoxHeader, guiBoxWidth - 2 * guiBoxBorder, h);
        GUILayout.BeginArea(areaRect);

        guiBoxY += totalH;
    }

    private void GuiEndBox()
    {
        GUILayout.EndArea();
    }

    private void GuiCaptionValue(string caption, string value)
    {
        GUILayout.BeginHorizontal();
        GUILayout.Label(caption);
        GUILayout.Label(value);
        GUILayout.EndHorizontal();
    }
}