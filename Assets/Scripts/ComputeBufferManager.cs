﻿using System;
using UnityEngine;

[ExecuteInEditMode]
public class ComputeBufferManager : MonoBehaviour
{
	public static int NumLodMax = 10;
	public static int NumProteinMax = 100;
	public static int NumProteinAtomMax = 3000000;
	public static int NumProteinAtomClusterMax = 300000;
	public static int NumProteinInstancesMax = 100000;
	public static int NumProteinSphereBatchesMax = 1000000;

	public static int NumCurveIngredientMax = 10;
	public static int NumCurveIngredientAtomsMax = 1000;
	public static int NumCurveControlPointsMax = 1000000;

	public static int NumMoleculeInstancesMax = 100000;
    public static int NumVisLinesMax = 100;

	public ComputeBuffer LodInfos;
	public ComputeBuffer OpaqueSphereBatchInfos;
    public ComputeBuffer TransparentSphereBatchInfos;

    // Protein buffers
    public ComputeBuffer ProteinColors;
	public ComputeBuffer ProteinToggleFlags;

	public ComputeBuffer ProteinAtoms;
	public ComputeBuffer ProteinAtomCount;
	public ComputeBuffer ProteinAtomStart;

	public ComputeBuffer ProteinAtomClusters;
	public ComputeBuffer ProteinAtomClusterCount;
	public ComputeBuffer ProteinAtomClusterStart;

	public ComputeBuffer ProteinInstanceInfos;
	public ComputeBuffer ProteinInstanceCullFlags;
	public ComputeBuffer ProteinInstancePositions;
	public ComputeBuffer ProteinInstanceRotations;

    public ComputeBuffer ProteinAdditionalInfos;

    // Curve ingredients buffers
    public ComputeBuffer CurveIngredientsInfos;
	public ComputeBuffer CurveIngredientsColors;
	public ComputeBuffer CurveIngredientsToggleFlags;

	public ComputeBuffer CurveIngredientsAtoms;
	public ComputeBuffer CurveIngredientsAtomCount;
	public ComputeBuffer CurveIngredientsAtomStart;

	public ComputeBuffer CurveControlPointsInfos;
	public ComputeBuffer CurveControlPointsNormals;
	public ComputeBuffer CurveControlPointsPositions;

    // Molecule buffers
    public ComputeBuffer MoleculeInstanceInfos;
    public ComputeBuffer MoleculeInstanceCullFlags;
    public ComputeBuffer MoleculeInstancePositions;
    public ComputeBuffer MoleculeInstanceRotations;

    public ComputeBuffer MoleculeAdditionalInfos;

    // Mutual instance buffer for composit shader
    public ComputeBuffer CombinedInstanceInfos;
    public ComputeBuffer CombinedInstancePositions;
    public ComputeBuffer CombinedInstanceRotations;

    // Buffer with ProteinInstancePositions for visualization
    public ComputeBuffer ProteinVisualizedPositions;

    public ComputeBuffer VisLines;

	//*****//

	// Declare the buffer manager as a singleton
	private static ComputeBufferManager _instance = null;
	public static ComputeBufferManager Instance
	{
		get
		{
			if (_instance == null)
			{
				_instance = FindObjectOfType<ComputeBufferManager>();
				if (_instance == null)
				{
					var go = GameObject.Find("_ComputeBufferManager");
					if (go != null)
						DestroyImmediate(go);

					go = new GameObject("_ComputeBufferManager") {hideFlags = HideFlags.HideInInspector};
					_instance = go.AddComponent<ComputeBufferManager>();
				}
			}

			return _instance;
		}
	}

	public ComputeBuffer ProteinDisplayPositions;
	public ComputeBuffer ProteinDisplayRotations;

	// Hack to clear append buffer
	public static void ClearAppendBuffer(ComputeBuffer appendBuffer)
	{
		// This resets the append buffer buffer to 0
		var dummy1 = RenderTexture.GetTemporary(8, 8, 24, RenderTextureFormat.ARGB32);
		var dummy2 = RenderTexture.GetTemporary(8, 8, 24, RenderTextureFormat.ARGB32);
		var active = RenderTexture.active;

		Graphics.SetRandomWriteTarget(1, appendBuffer);
		Graphics.Blit(dummy1, dummy2);
		Graphics.ClearRandomWriteTargets();

		RenderTexture.active = active;

		dummy1.Release();
		dummy2.Release();
	}

	void OnEnable()
	{
        InitBuffers();
    }

	void OnDisable()
	{
		ReleaseBuffers();
	}

	public void InitBuffers()
	{
		if (LodInfos == null) LodInfos = new ComputeBuffer(8, 16);
        if (OpaqueSphereBatchInfos == null) OpaqueSphereBatchInfos = new ComputeBuffer(NumProteinSphereBatchesMax, 16, ComputeBufferType.Append);
        if (TransparentSphereBatchInfos == null) TransparentSphereBatchInfos = new ComputeBuffer(NumProteinSphereBatchesMax, 16, ComputeBufferType.Append);

        //*****//

        if (ProteinColors == null) ProteinColors = new ComputeBuffer(NumProteinMax, 16);
		if (ProteinToggleFlags == null) ProteinToggleFlags = new ComputeBuffer(NumProteinMax, 4);

		if (ProteinAtoms == null) ProteinAtoms = new ComputeBuffer(NumProteinAtomMax, 16);
		if (ProteinAtomClusters == null) ProteinAtomClusters = new ComputeBuffer(NumProteinAtomClusterMax, 16);

		if (ProteinAtomCount == null) ProteinAtomCount = new ComputeBuffer(NumProteinMax, 4);
		if (ProteinAtomStart == null) ProteinAtomStart = new ComputeBuffer(NumProteinMax, 4);
		if (ProteinAtomClusterCount == null) ProteinAtomClusterCount = new ComputeBuffer(NumProteinMax * NumLodMax, 4);
		if (ProteinAtomClusterStart == null) ProteinAtomClusterStart = new ComputeBuffer(NumProteinMax * NumLodMax, 4);

		if (ProteinInstanceInfos == null) ProteinInstanceInfos = new ComputeBuffer(NumProteinInstancesMax, 16);
		if (ProteinInstanceCullFlags == null) ProteinInstanceCullFlags = new ComputeBuffer(NumProteinInstancesMax, 4);
		if (ProteinInstancePositions == null) ProteinInstancePositions = new ComputeBuffer(NumProteinInstancesMax, 16);
		if (ProteinInstanceRotations == null) ProteinInstanceRotations = new ComputeBuffer(NumProteinInstancesMax, 16);

        if (MoleculeInstanceInfos == null) MoleculeInstanceInfos = new ComputeBuffer(NumProteinInstancesMax, 16);
        if (MoleculeInstanceCullFlags == null) MoleculeInstanceCullFlags = new ComputeBuffer(NumProteinInstancesMax, 4);
        if (MoleculeInstancePositions == null) MoleculeInstancePositions = new ComputeBuffer(NumProteinInstancesMax, 16);
        if (MoleculeInstanceRotations == null) MoleculeInstanceRotations = new ComputeBuffer(NumProteinInstancesMax, 16);

        if (ProteinDisplayPositions == null) ProteinDisplayPositions = new ComputeBuffer(NumProteinInstancesMax, 16);
		if (ProteinDisplayRotations == null) ProteinDisplayRotations = new ComputeBuffer(NumProteinInstancesMax, 16);

        if (ProteinAdditionalInfos == null) ProteinAdditionalInfos = new ComputeBuffer(NumProteinInstancesMax, 8);
        if (MoleculeAdditionalInfos == null) MoleculeAdditionalInfos = new ComputeBuffer(NumProteinInstancesMax, 8);

        //*****//

        if (CurveIngredientsInfos == null) CurveIngredientsInfos = new ComputeBuffer(NumCurveIngredientMax, 16);
		if (CurveIngredientsColors == null) CurveIngredientsColors = new ComputeBuffer(NumCurveIngredientMax, 16);
		if (CurveIngredientsToggleFlags == null) CurveIngredientsToggleFlags = new ComputeBuffer(NumCurveIngredientMax, 4);

		if (CurveIngredientsAtomCount == null) CurveIngredientsAtomCount = new ComputeBuffer(NumCurveIngredientMax, 4);
		if (CurveIngredientsAtomStart == null) CurveIngredientsAtomStart = new ComputeBuffer(NumCurveIngredientMax, 4);
		if (CurveIngredientsAtoms == null) CurveIngredientsAtoms = new ComputeBuffer(NumCurveIngredientAtomsMax, 16);

		if (CurveControlPointsInfos == null) CurveControlPointsInfos = new ComputeBuffer(NumCurveControlPointsMax, 16);
		if (CurveControlPointsNormals == null) CurveControlPointsNormals = new ComputeBuffer(NumCurveControlPointsMax, 16);
		if (CurveControlPointsPositions == null) CurveControlPointsPositions = new ComputeBuffer(NumCurveControlPointsMax, 16);

		//*****//
		
		if (CombinedInstanceInfos == null) CombinedInstanceInfos = new ComputeBuffer(NumProteinInstancesMax, 16);
		if (CombinedInstancePositions == null) CombinedInstancePositions = new ComputeBuffer(NumProteinInstancesMax, 16);
		if (CombinedInstanceRotations == null) CombinedInstanceRotations = new ComputeBuffer(NumProteinInstancesMax, 16);

        //*****//

        if (ProteinVisualizedPositions == null) ProteinVisualizedPositions = new ComputeBuffer(NumProteinInstancesMax, 16);

        //*****//

        if (VisLines == null) VisLines = new ComputeBuffer(NumProteinInstancesMax, 48, ComputeBufferType.Append);
    }
	
	// Flush buffers on exit
	void ReleaseBuffers ()
	{
		if (LodInfos != null) { LodInfos.Release(); LodInfos = null; }
        if (OpaqueSphereBatchInfos != null) { OpaqueSphereBatchInfos.Release(); OpaqueSphereBatchInfos = null; }
        if (TransparentSphereBatchInfos != null) { TransparentSphereBatchInfos.Release(); TransparentSphereBatchInfos = null; }

        //*****//

        if (ProteinColors != null) { ProteinColors.Release(); ProteinColors = null; }
		if (ProteinToggleFlags != null) { ProteinToggleFlags.Release(); ProteinToggleFlags = null; }
		
		if (ProteinAtoms != null) { ProteinAtoms.Release(); ProteinAtoms = null; }
		if (ProteinAtomCount != null) { ProteinAtomCount.Release(); ProteinAtomCount = null; }
		if (ProteinAtomStart != null) { ProteinAtomStart.Release(); ProteinAtomStart = null; }
		
		if (ProteinAtomClusters != null) { ProteinAtomClusters.Release(); ProteinAtomClusters = null; }
		if (ProteinAtomClusterCount != null) { ProteinAtomClusterCount.Release(); ProteinAtomClusterCount = null; }
		if (ProteinAtomClusterStart != null) { ProteinAtomClusterStart.Release(); ProteinAtomClusterStart = null; }

		if (ProteinInstanceInfos != null) { ProteinInstanceInfos.Release(); ProteinInstanceInfos = null; }
		if (ProteinInstanceCullFlags != null) { ProteinInstanceCullFlags.Release(); ProteinInstanceCullFlags = null; }
		if (ProteinInstancePositions != null) { ProteinInstancePositions.Release(); ProteinInstancePositions = null; }
		if (ProteinInstanceRotations != null) { ProteinInstanceRotations.Release(); ProteinInstanceRotations = null; }

        if (MoleculeInstanceInfos != null) { MoleculeInstanceInfos.Release(); MoleculeInstanceInfos = null; }
        if (MoleculeInstanceCullFlags != null) { MoleculeInstanceCullFlags.Release(); MoleculeInstanceCullFlags = null; }
        if (MoleculeInstancePositions != null) { MoleculeInstancePositions.Release(); MoleculeInstancePositions = null; }
        if (MoleculeInstanceRotations != null) { MoleculeInstanceRotations.Release(); MoleculeInstanceRotations = null; }

        if (ProteinDisplayPositions != null) { ProteinDisplayPositions.Release(); ProteinDisplayPositions = null; }
		if (ProteinDisplayRotations != null) { ProteinDisplayRotations.Release(); ProteinDisplayRotations = null; }

        if (ProteinAdditionalInfos != null) { ProteinAdditionalInfos.Release(); ProteinAdditionalInfos = null; }
        if (MoleculeAdditionalInfos != null) { MoleculeAdditionalInfos.Release(); MoleculeAdditionalInfos = null; }

        //*****//

        if (CurveIngredientsInfos != null) { CurveIngredientsInfos.Release(); CurveIngredientsInfos = null; }
		if (CurveIngredientsColors != null) { CurveIngredientsColors.Release(); CurveIngredientsColors = null; }
		if (CurveIngredientsToggleFlags != null) { CurveIngredientsToggleFlags.Release(); CurveIngredientsToggleFlags = null; }

		if (CurveIngredientsAtoms != null) { CurveIngredientsAtoms.Release(); CurveIngredientsAtoms = null; }
		if (CurveIngredientsAtomCount != null) { CurveIngredientsAtomCount.Release(); CurveIngredientsAtomCount = null; }
		if (CurveIngredientsAtomStart != null) { CurveIngredientsAtomStart.Release(); CurveIngredientsAtomStart = null; }

		if (CurveControlPointsInfos != null) { CurveControlPointsInfos.Release(); CurveControlPointsInfos = null; }
		if (CurveControlPointsNormals != null) { CurveControlPointsNormals.Release(); CurveControlPointsNormals = null; }
		if (CurveControlPointsPositions != null) { CurveControlPointsPositions.Release(); CurveControlPointsPositions = null; }

        //*****//

        if (CombinedInstanceInfos != null) { CombinedInstanceInfos.Release(); CombinedInstanceInfos = null; }
        if (CombinedInstancePositions != null) { CombinedInstancePositions.Release(); CombinedInstancePositions = null; }
        if (CombinedInstanceRotations != null) { CombinedInstanceRotations.Release(); CombinedInstanceRotations = null; }

        //*****//

        if (ProteinVisualizedPositions != null) { ProteinVisualizedPositions.Release(); ProteinVisualizedPositions = null; }

        //*****//

        if (VisLines != null) { VisLines.Release(); VisLines = null; }
    }
}
