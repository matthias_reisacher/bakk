using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

enum InstanceState
{
    Null = -1,           // Instance will not be displayed
    Normal = 0,          // Instance will be displayed with normal color
    Highlighted = 1,     // Instance will be displayed with highlighted color
    Grayscale = 2        // Instance will be displayed with gray color
};

[ExecuteInEditMode]
public class SceneManager : MonoBehaviour
{
    // Scene data
    public List<Vector4> ProteinInstanceInfos = new List<Vector4>();
    public List<Vector4> ProteinInstancePositions = new List<Vector4>();
    public List<Vector4> ProteinInstanceRotations = new List<Vector4>();

    public List<Vector4> CurveControlPointsInfos = new List<Vector4>();
    public List<Vector4> CurveControlPointsNormals = new List<Vector4>();
    public List<Vector4> CurveControlPointsPositions = new List<Vector4>();

    public List<Vector4> MoleculeInstanceInfos = new List<Vector4>();
    public List<Vector4> MoleculeInstancePositions = new List<Vector4>();
    public List<Vector4> MoleculeInstanceRotations = new List<Vector4>();

    // Protein ingredients data

    public List<int> ProteinAtomCount = new List<int>();
    public List<int> ProteinAtomStart = new List<int>();
    public List<int> ProteinToggleFlags = new List<int>();
    public List<string> ProteinNames = new List<string>();
    public List<Vector4> ProteinAtoms = new List<Vector4>();
    public List<Vector4> ProteinColors = new List<Vector4>();
    public List<float> ProteinBoundingSpheres = new List<float>();
    public List<Vector4> ProteinAtomClusters = new List<Vector4>();
    public List<int> ProteinAtomClusterCount = new List<int>();
    public List<int> ProteinAtomClusterStart = new List<int>();

    // Curve ingredients data
    
    public List<int> CurveIngredientsAtomStart = new List<int>();
    public List<int> CurveIngredientsAtomCount = new List<int>();
    public List<int> CurveIngredientToggleFlags = new List<int>();
    public List<string> CurveIngredientsNames = new List<string>(); 
    public List<Vector4> CurveIngredientsAtoms = new List<Vector4>();
    public List<Vector4> CurveIngredientsInfos = new List<Vector4>();
    public List<Vector4> CurveIngredientsColors = new List<Vector4>();

    //--------------------------------------------------------------

    public Vector3 lazyControlPosition;
    private bool lazyControlEnabled = false;
    private float lazyControlDisableTime = 1;
    private float lazyControlLastUpdateTime = 0;
    private float lazyControlDistance = 25;

    //--------------------------------------------------------------

    public int SelectedElement = -1;
    private InstanceState SelectedElementOldState;

    private Vector4 _minInstancePosition = new Vector4(float.MaxValue, float.MaxValue, float.MaxValue, float.MaxValue);
    public Vector4 MinInstancePosition { get { return _minInstancePosition; } }
    private Vector4 _maxInstancePosition = new Vector4(float.MinValue, float.MinValue, float.MinValue, float.MinValue);
    public Vector4 MaxInstancePosition { get { return _maxInstancePosition; } }

    public int NumLodLevels = 0;
    public int TotalNumProteinAtoms = 0;
    public int TotalNumMoleculeAtoms = 0;

    public int NumMoleculeInstances = 0;
    public int NumMoleculeSpecies = 0;
    public int FirstMoleculeId = -1;

    public int NumProteinInstances
    {
        get { return ProteinInstancePositions.Count; }
    }

    public int NumDnaControlPoints
    {
        get { return CurveControlPointsPositions.Count; }
    }

    public int NumDnaSegments
    {
        get { return Math.Max(CurveControlPointsPositions.Count - 1, 0); }
    }

    //--------------------------------------------------------------
    // Declare the scene manager as a singleton
    private static SceneManager _instance = null;
    public static SceneManager Instance
    {
        get
        {
            if (_instance != null) return _instance;

            _instance = FindObjectOfType<SceneManager>();
            if (_instance == null)
            {
                var go = GameObject.Find("_SceneManager");
                if (go != null) DestroyImmediate(go);

                go = new GameObject("_SceneManager") { hideFlags = HideFlags.HideInInspector };
                _instance = go.AddComponent<SceneManager>();
            }
            
            _instance.OnUnityReload();

            return _instance;
        }
    }

    //--------------------------------------------------------------
    
    void Update()
    {
        // Break if simulation is stopped.
        if (SimulationManager.IsSimulating && SimulationManager.IsPaused)
            return;

        DoBrownianMotion(ComputeBufferManager.Instance.ProteinInstancePositions);
    }

    void DoBrownianMotion(ComputeBuffer instancePosition)
    {
        ComputeShaderManager.Instance.BrownianMotionCS.SetInt("_Enable", Application.isPlaying ? Convert.ToInt32(PersistantSettings.Instance.EnableBrownianMotion) : 0);
        ComputeShaderManager.Instance.BrownianMotionCS.SetFloat("_Time", Time.realtimeSinceStartup);
        ComputeShaderManager.Instance.BrownianMotionCS.SetFloat("_SpeedFactor", PersistantSettings.Instance.SpeedFactor);
        ComputeShaderManager.Instance.BrownianMotionCS.SetFloat("_MoveFactor", PersistantSettings.Instance.MoveFactor);
        ComputeShaderManager.Instance.BrownianMotionCS.SetFloat("_RotateFactor", PersistantSettings.Instance.RotateFactor);

        // Do brownian motion for proteins
        int kernel = ComputeShaderManager.Instance.BrownianMotionCS.FindKernel("Proteins");
        ComputeShaderManager.Instance.BrownianMotionCS.SetBuffer(kernel, "_CullFlags", ComputeBufferManager.Instance.ProteinInstanceCullFlags);
        ComputeShaderManager.Instance.BrownianMotionCS.SetBuffer(kernel, "_ProteinInstancePositions", instancePosition);
        ComputeShaderManager.Instance.BrownianMotionCS.SetBuffer(kernel, "_ProteinInstanceRotations", ComputeBufferManager.Instance.ProteinInstanceRotations);
        ComputeShaderManager.Instance.BrownianMotionCS.SetBuffer(kernel, "_ProteinInstanceDisplayPositions", ComputeBufferManager.Instance.ProteinDisplayPositions);
        ComputeShaderManager.Instance.BrownianMotionCS.SetBuffer(kernel, "_ProteinInstanceDisplayRotations", ComputeBufferManager.Instance.ProteinDisplayRotations);
        ComputeShaderManager.Instance.BrownianMotionCS.Dispatch(kernel, NumProteinInstances, 1, 1);

        // Do brownian motion for molecules
        kernel = ComputeShaderManager.Instance.BrownianMotionCS.FindKernel("Molecules");
        ComputeShaderManager.Instance.BrownianMotionCS.SetBuffer(kernel, "_CullFlags", ComputeBufferManager.Instance.MoleculeInstanceCullFlags);
        ComputeShaderManager.Instance.BrownianMotionCS.SetBuffer(kernel, "_MoleculeInstancePositions", ComputeBufferManager.Instance.MoleculeInstancePositions);
        ComputeShaderManager.Instance.BrownianMotionCS.SetBuffer(kernel, "_MoleculeInstanceRotations", ComputeBufferManager.Instance.MoleculeInstanceRotations);
        ComputeShaderManager.Instance.BrownianMotionCS.Dispatch(kernel, NumMoleculeInstances, 1, 1);
    }

    void OnEnable()
    {
        PersistantSettings.Instance.EnableBrownianMotion = true;
        CalcMinMaxInstancePosition();
        lazyControlLastUpdateTime = Time.realtimeSinceStartup;
    }

    //--------------------------------------------------------------

    #region Protein_functions

    public int AddIngredient(string ingredientName, Bounds bounds, List<Vector4> atomSpheres, Color color, List<float> clusterLevels = null, bool isMolecule = false)
    {
        if (ProteinNames.Contains(ingredientName))
        {
            return ProteinNames.IndexOf(ingredientName);
        }

        if (NumLodLevels != 0 && NumLodLevels != clusterLevels.Count)
            throw new Exception("Uneven cluster levels number: " + ingredientName);

        if(isMolecule)
        {
            NumMoleculeSpecies++;
            if (FirstMoleculeId == -1) FirstMoleculeId = ProteinNames.Count;
        }

        if (color == null) { color = Helper.GetRandomColor(); }
        
        ProteinNames.Add(ingredientName);
        ProteinColors.Add(color);
        ProteinToggleFlags.Add(1);
        ProteinBoundingSpheres.Add(Vector3.Magnitude(bounds.extents));

        ProteinAtomCount.Add(atomSpheres.Count);
        ProteinAtomStart.Add(ProteinAtoms.Count);
        ProteinAtoms.AddRange(atomSpheres);

        if (clusterLevels != null)
        {
            NumLodLevels = clusterLevels.Count;

            foreach (var level in clusterLevels)
            {
                var numClusters = Math.Max(atomSpheres.Count * level, 5);
                var clusterSpheres = KMeansClustering.GetClusters(atomSpheres, (int)numClusters);

                ProteinAtomClusterCount.Add(clusterSpheres.Count);
                ProteinAtomClusterStart.Add(ProteinAtomClusters.Count);
                ProteinAtomClusters.AddRange(clusterSpheres);
            }
        }

        return ProteinNames.IndexOf(ingredientName);
    }

    public void AddIngredientInstance(string ingredientName, Vector3 position, Quaternion rotation, int unitId = 0)
    {
        if (!ProteinNames.Contains(ingredientName))
        {
            throw new Exception("Ingredient type do not exists");
        }

        var ingredientId = ProteinNames.IndexOf(ingredientName);

        Vector4 instancePosition = position;
        instancePosition.w = ProteinBoundingSpheres[ingredientId];

        ProteinInstanceInfos.Add(new Vector4(ingredientId, (int)InstanceState.Normal, unitId, 1));
        ProteinInstancePositions.Add(instancePosition);
        ProteinInstanceRotations.Add(Helper.QuanternionToVector4(rotation));
        
        TotalNumProteinAtoms += ProteinAtomCount[ingredientId];
    }

    // Add molecules instances for reaction purpose.
    public void AddMoleculeInstance(string ingredientName, Vector3 position, Quaternion rotation, int unitId = 0)
    {
        if (!ProteinNames.Contains(ingredientName))
        {
            throw new Exception("Ingredient type do not exists");
        }

        var ingredientId = ProteinNames.IndexOf(ingredientName);

        Vector4 instancePosition = position;
        instancePosition.w = ProteinBoundingSpheres[ingredientId];
        
        MoleculeInstanceInfos.Add(new Vector4(ingredientId, (int)InstanceState.Grayscale, unitId, 1));
        MoleculeInstancePositions.Add(instancePosition);
        MoleculeInstanceRotations.Add(Helper.QuanternionToVector4(rotation));
        
        TotalNumMoleculeAtoms += ProteinAtomCount[ingredientId];
    }

    #endregion

    #region Curve_functions

    public void AddCurveIngredient(string name, string pdbName)
    {
        if (ProteinNames.Contains(name)) return;
        
        int numSteps = 1;
        float twistAngle = 0;
        float segmentLength = 34.0f;
        var color = Helper.GetRandomColor();

        if (name.Contains("DNA"))
        {
            numSteps = 12;
            twistAngle = 34.3f;
            segmentLength = 34.0f;
            color = Color.yellow;

            var atomSpheres = PdbLoader.LoadAtomSpheres(pdbName);
            CurveIngredientsAtomCount.Add(atomSpheres.Count);
            CurveIngredientsAtomStart.Add(CurveIngredientsAtoms.Count);
            CurveIngredientsAtoms.AddRange(atomSpheres);
        }
        else if (name.Contains("mRNA"))
        {
            numSteps = 12;
            twistAngle = 34.3f;
            segmentLength = 34.0f;
            color = Color.red;

            var atomSpheres = PdbLoader.LoadAtomSpheres(pdbName);
            CurveIngredientsAtomCount.Add(atomSpheres.Count);
            CurveIngredientsAtomStart.Add(CurveIngredientsAtoms.Count);
            CurveIngredientsAtoms.AddRange(atomSpheres);
        }
        else if (name.Contains("peptide"))
        {
            numSteps = 10;
            twistAngle = 0;
            segmentLength = 20.0f;
            color = Color.magenta;

            var atomSphere = new Vector4(0,0,0,3);
            CurveIngredientsAtomCount.Add(1);
            CurveIngredientsAtomStart.Add(CurveIngredientsAtoms.Count);
            CurveIngredientsAtoms.Add(atomSphere);
        }
        else if (name.Contains("lypoglycane"))
        {
            numSteps = 10;
            twistAngle = 0;
            segmentLength = 20;
            color = Color.green;

            var atomSphere = new Vector4(0, 0, 0, 8);
            CurveIngredientsAtomCount.Add(1);
            CurveIngredientsAtomStart.Add(CurveIngredientsAtoms.Count);
            CurveIngredientsAtoms.Add(atomSphere);
        }
        else
        {
            throw new Exception("Curve ingredient unknown");
        }

        CurveIngredientsNames.Add(name);
        CurveIngredientsColors.Add(color);
        CurveIngredientToggleFlags.Add(1);
        CurveIngredientsInfos.Add(new Vector4(numSteps, twistAngle, segmentLength));
    }

    public void AddCurve(string name, List<Vector4> path)
    {
        if (!CurveIngredientsNames.Contains(name))
        {
            throw new Exception("Curve ingredient type do not exists");
        }

        var curveIngredientId = CurveIngredientsNames.IndexOf(name);
        var positions = ResampleControlPoints(path, CurveIngredientsInfos[curveIngredientId].z);
        var normals = GetSmoothNormals(positions);

        var curveId = CurveControlPointsPositions.Count;
        var curveType = CurveIngredientsNames.IndexOf(name);

        for (int i = 0; i < positions.Count; i++)
        {
            CurveControlPointsInfos.Add(new Vector4(curveId, curveType, 0, 0));
        }

        CurveControlPointsNormals.AddRange(normals);
        CurveControlPointsPositions.AddRange(positions);

        //Debug.Log(positions.Count);
    }
    
    private List<Vector4> ResampleControlPoints(List<Vector4> controlPoints, float segmentLength)
    {
        int nP = controlPoints.Count;
        //insert a point at the end and at the begining
        controlPoints.Insert(0, controlPoints[0] + (controlPoints[0] - controlPoints[1]) / 2.0f);
        controlPoints.Add(controlPoints[nP - 1] + (controlPoints[nP - 1] - controlPoints[nP - 2]) / 2.0f);

        var resampledControlPoints = new List<Vector4>();
        resampledControlPoints.Add(controlPoints[0]);
        resampledControlPoints.Add(controlPoints[1]);

        var currentPointId = 1;
        var currentPosition = controlPoints[currentPointId];

        //distance = DisplaySettings.Instance.DistanceContraint;
        float lerpValue = 0.0f;

        // Normalize the distance between control points
        while (true)
        {
            if (currentPointId + 2 >= controlPoints.Count) break;
            //if (currentPointId + 2 >= 100) break;

            var cp0 = controlPoints[currentPointId - 1];
            var cp1 = controlPoints[currentPointId];
            var cp2 = controlPoints[currentPointId + 1];
            var cp3 = controlPoints[currentPointId + 2];

            var found = false;

            for (; lerpValue <= 1; lerpValue += 0.01f)
            {
                var candidate = Helper.CubicInterpolate(cp0, cp1, cp2, cp3, lerpValue);
                var d = Vector3.Distance(currentPosition, candidate);

                if (d > segmentLength)
                {
                    resampledControlPoints.Add(candidate);
                    currentPosition = candidate;
                    found = true;
                    break;
                }
            }

            if (!found)
            {
                lerpValue = 0;
                currentPointId++;
            }
        }

        return resampledControlPoints;
    }

    public List<Vector4> GetSmoothNormals(List<Vector4> controlPoints)
    {
        var smoothNormals = new List<Vector4>();
        var crossDirection = Vector3.up;

        var p0 = controlPoints[0];
        var p1 = controlPoints[1];
        var p2 = controlPoints[2];

        smoothNormals.Add(Vector3.Normalize(Vector3.Cross(p0 - p1, p2 - p1)));

        for (int i = 1; i < controlPoints.Count - 1; i++)
        {
            p0 = controlPoints[i - 1];
            p1 = controlPoints[i];
            p2 = controlPoints[i + 1];

            var t = Vector3.Normalize(p2 - p0);
            var b = Vector3.Normalize(Vector3.Cross(t, smoothNormals.Last()));
            var n = -Vector3.Normalize(Vector3.Cross(t, b));

            smoothNormals.Add(n);
        }

        smoothNormals.Add(controlPoints.Last());

        return smoothNormals;
    }

    #endregion

    #region Misc_functions

    public void UpdateLazyControl(Vector3 newPos)
    {
        float timeNow = Time.realtimeSinceStartup;

        // If position has not changed, return.
        if (lazyControlPosition == newPos)
        {
            if (timeNow - lazyControlLastUpdateTime > lazyControlDisableTime)
                lazyControlEnabled = false;
            return;
        }

        // If lazy control is disabled, check if distance is large enough to enable lazy control.
        if(!lazyControlEnabled)
        {
            float distance = (lazyControlPosition - newPos).magnitude;

            // Calculate distance from newPos to selected element divided by 100 to inlcude 
            Vector4 target = GetSelectedElementPosition();
            float ratio = (newPos - (new Vector3(target.x, target.y, target.z) * PersistantSettings.Instance.Scale)).magnitude / 100;

            if (distance >= lazyControlDistance * ratio)
                lazyControlEnabled = true;
        }

        // Update base if lazy control is enabled
        if (lazyControlEnabled)
        {
            lazyControlPosition = newPos;
            lazyControlLastUpdateTime = timeNow;
        }
    }

    public void CalcMinMaxInstancePosition()
    {
        _minInstancePosition = new Vector4(float.MaxValue, float.MaxValue, float.MaxValue, float.MaxValue);
        _maxInstancePosition = new Vector4(float.MinValue, float.MinValue, float.MinValue, float.MinValue);

        for(int i = 0; i < ProteinInstancePositions.Count; i++)
        {
            Vector4 pos = ProteinInstancePositions[i];
            _minInstancePosition = Vector4.Min(pos, _minInstancePosition);
            _maxInstancePosition = Vector4.Max(pos, _maxInstancePosition);
        }
    }

    public void SetSelectedElement(int elementId)
    {
        //Debug.Log("Selected element id: " + elementId);
        if(SelectedElement != -1)   UpdateInstanceState(SelectedElement, (int) SelectedElementOldState);
        SelectedElement = elementId;
        SelectedElementOldState = (InstanceState) GetInstanceState(SelectedElement);
        UpdateInstanceState(SelectedElement, (int)InstanceState.Highlighted);
    }

    public Vector4 GetSelectedElementPosition()
    {
        if (SelectedElement == -1)
            return new Vector4(0, 0, 0, 0);
        else
        {
            if (SelectedElement < NumProteinInstances)
                return ProteinInstancePositions[SelectedElement];
            else
                return MoleculeInstancePositions[SelectedElement - NumProteinInstances];
        }
    }

    // Returns the instance state of a protein or a molecule instance.
    private int GetInstanceState(int id)
    {
        int kernel = ComputeShaderManager.Instance.InstanceStateOperations.FindKernel("GetProteinState");
        var outBuffer = new ComputeBuffer(1, sizeof(int));
        ComputeShaderManager.Instance.InstanceStateOperations.SetBuffer(kernel, "_OutputBuffer", outBuffer);

        if (id < NumProteinInstances)
        {
            ComputeShaderManager.Instance.InstanceStateOperations.SetInt("_Id", id);
            ComputeShaderManager.Instance.InstanceStateOperations.SetBuffer(kernel, "_InstanceInfos", ComputeBufferManager.Instance.ProteinInstanceInfos);
        }
        else
        {
            ComputeShaderManager.Instance.InstanceStateOperations.SetInt("_Id", id - NumProteinInstances);
            ComputeShaderManager.Instance.InstanceStateOperations.SetBuffer(kernel, "_InstanceData", ComputeBufferManager.Instance.MoleculeInstanceInfos);
        }

        ComputeShaderManager.Instance.InstanceStateOperations.Dispatch(kernel, 1, 1, 1);

        var state = new[] { 0 };
        outBuffer.GetData(state);
        outBuffer.Release();

        return state[0];
    }

    // Updates the instance state of a protein or a molecule instance.
    private void UpdateInstanceState(int id, int newState)
    {
        int kernel = ComputeShaderManager.Instance.InstanceStateOperations.FindKernel("UpdateProteinState");
        ComputeShaderManager.Instance.InstanceStateOperations.SetInt("_NewInstanceState", newState);

        if (id < NumProteinInstances)
        {
            ComputeShaderManager.Instance.InstanceStateOperations.SetInt("_Id", id);
            ComputeShaderManager.Instance.InstanceStateOperations.SetBuffer(kernel, "_InstanceInfos", ComputeBufferManager.Instance.ProteinInstanceInfos);
        }
        else
        {
            ComputeShaderManager.Instance.InstanceStateOperations.SetInt("_Id", id - NumProteinInstances);
            ComputeShaderManager.Instance.InstanceStateOperations.SetBuffer(kernel, "_InstanceData", ComputeBufferManager.Instance.MoleculeInstanceInfos);
        }

        ComputeShaderManager.Instance.InstanceStateOperations.Dispatch(kernel, 1, 1, 1);
    }

    private void OnUnityReload()
    {
        Debug.Log("Reload Scene");
        SelectedElement = -1;
        //_instance.ClearScene();
        _instance.UploadAllData();
        _instance.DoCrossSectionTest();
    }

    public void DoCrossSectionTest()
    {
        int kernel = ComputeShaderManager.Instance.CrossSection.FindKernel("CrossSection");
        ComputeShaderManager.Instance.CrossSection.SetFloat("_Scale", PersistantSettings.Instance.Scale);
        ComputeShaderManager.Instance.CrossSection.SetInt("_EnableCrossSection", Convert.ToInt32(PersistantSettings.Instance.EnableCrossSection));
        ComputeShaderManager.Instance.CrossSection.SetVector("_CrossSectionPlane", new Vector4(PersistantSettings.Instance.CrossSectionPlaneNormal.x, PersistantSettings.Instance.CrossSectionPlaneNormal.y, PersistantSettings.Instance.CrossSectionPlaneNormal.z, PersistantSettings.Instance.CrossSectionPlaneDistance));

        // Do protein test
        ComputeShaderManager.Instance.CrossSection.SetBuffer(kernel, "_CullFlags", ComputeBufferManager.Instance.ProteinInstanceCullFlags);
        ComputeShaderManager.Instance.CrossSection.SetBuffer(kernel, "_Positions", ComputeBufferManager.Instance.ProteinInstancePositions);
        ComputeShaderManager.Instance.CrossSection.Dispatch(kernel, NumProteinInstances, 1, 1);

        // Do molecule test
        ComputeShaderManager.Instance.CrossSection.SetBuffer(kernel, "_CullFlags", ComputeBufferManager.Instance.MoleculeInstanceCullFlags);
        ComputeShaderManager.Instance.CrossSection.SetBuffer(kernel, "_Positions", ComputeBufferManager.Instance.MoleculeInstancePositions);
        ComputeShaderManager.Instance.CrossSection.Dispatch(kernel, NumMoleculeInstances, 1, 1);
    }

    // Scene data gets serialized on each reload, to clear the scene call this function
    public void ClearScene()
    {
        Debug.Log("Clear Scene");

        NumLodLevels = 0;
        SelectedElement = -1;
        TotalNumProteinAtoms = 0;

        // Clear scene data
        ProteinInstanceInfos.Clear();
        ProteinInstancePositions.Clear();
        ProteinInstanceRotations.Clear();

        ClearMolecules();

        // Clear ingredient data
        ProteinNames.Clear();
        ProteinColors.Clear();
        ProteinToggleFlags.Clear();
        ProteinBoundingSpheres.Clear();

        // Clear atom data
        ProteinAtoms.Clear();
        ProteinAtomCount.Clear();
        ProteinAtomStart.Clear();

        // Clear cluster data
        ProteinAtomClusters.Clear();
        ProteinAtomClusterStart.Clear();
        ProteinAtomClusterCount.Clear();

        // Clear curve data
        CurveIngredientsInfos.Clear();
        CurveIngredientsNames.Clear();
        CurveIngredientsColors.Clear();
        CurveIngredientToggleFlags.Clear();
        CurveIngredientsAtoms.Clear();
        CurveIngredientsAtomCount.Clear();
        CurveIngredientsAtomStart.Clear();
        
        CurveControlPointsPositions.Clear();
        CurveControlPointsNormals.Clear();
        CurveControlPointsInfos.Clear();
    }

    public void ClearMolecules()
    {
        // Clear molecule instances
        MoleculeInstanceInfos.Clear();
        MoleculeInstancePositions.Clear();
        MoleculeInstanceRotations.Clear();
    }

    public void UploadAllData()
    {
        //ComputeBufferManager.Instance.InitBuffers();
        ComputeBufferManager.Instance.LodInfos.SetData(PersistantSettings.Instance.LodLevels);

        // Upload ingredient data
        UploadProteinData();
        UploadProteinInstanceData();
        UploadMoleculeInstanceData();
        UploadCurveIngredientData();
    }

    public void UploadProteinData()
    {
        // Check Buffer sizes
        if (Instance.NumLodLevels >= ComputeBufferManager.NumLodMax) throw new Exception("GPU buffer overflow");
        if (Instance.ProteinNames.Count >= ComputeBufferManager.NumProteinMax) throw new Exception("GPU buffer overflow");
        if (Instance.ProteinAtoms.Count >= ComputeBufferManager.NumProteinAtomMax) throw new Exception("GPU buffer overflow");
        if (Instance.ProteinAtomClusters.Count >= ComputeBufferManager.NumProteinAtomClusterMax) throw new Exception("GPU buffer overflow");
        if (Instance.ProteinAtomClusterCount.Count >= ComputeBufferManager.NumProteinMax * ComputeBufferManager.NumLodMax) throw new Exception("GPU buffer overflow");

        // Upload data
        ComputeBufferManager.Instance.ProteinColors.SetData(ProteinColors.ToArray());
        ComputeBufferManager.Instance.ProteinToggleFlags.SetData(ProteinToggleFlags.ToArray());
        ComputeBufferManager.Instance.ProteinAtoms.SetData(ProteinAtoms.ToArray());
        ComputeBufferManager.Instance.ProteinAtomCount.SetData(ProteinAtomCount.ToArray());
        ComputeBufferManager.Instance.ProteinAtomStart.SetData(ProteinAtomStart.ToArray());
        ComputeBufferManager.Instance.ProteinAtomClusters.SetData(ProteinAtomClusters.ToArray());
        ComputeBufferManager.Instance.ProteinAtomClusterCount.SetData(ProteinAtomClusterCount.ToArray());
        ComputeBufferManager.Instance.ProteinAtomClusterStart.SetData(ProteinAtomClusterStart.ToArray());
    }

    public void UploadProteinInstanceData()
    {
        // Check Buffer sizes
        if (Instance.ProteinInstancePositions.Count >= ComputeBufferManager.NumProteinInstancesMax) throw new Exception("GPU buffer overflow");

        // Upload data
        ComputeBufferManager.Instance.ProteinInstanceInfos.SetData(ProteinInstanceInfos.ToArray());
        ComputeBufferManager.Instance.ProteinInstancePositions.SetData(ProteinInstancePositions.ToArray());
        ComputeBufferManager.Instance.ProteinInstanceRotations.SetData(ProteinInstanceRotations.ToArray());
    }

    public void UploadMoleculeInstanceData()
    {
        // Check Buffer sizes
        if (Instance.MoleculeInstancePositions.Count >= ComputeBufferManager.NumMoleculeInstancesMax) throw new Exception("GPU buffer overflow");

        // Upload data
        ComputeBufferManager.Instance.MoleculeInstanceInfos.SetData(MoleculeInstanceInfos.ToArray());
        ComputeBufferManager.Instance.MoleculeInstancePositions.SetData(MoleculeInstancePositions.ToArray());
        ComputeBufferManager.Instance.MoleculeInstanceRotations.SetData(MoleculeInstanceRotations.ToArray());

        // Set counter
        NumMoleculeInstances = MoleculeInstancePositions.Count;
    }

    public void UploadCurveIngredientData()
    {
        // Check Buffer sizes
        if (Instance.CurveIngredientsNames.Count >= ComputeBufferManager.NumCurveIngredientMax) throw new Exception("GPU buffer overflow");
        if (Instance.CurveControlPointsPositions.Count >= ComputeBufferManager.NumCurveControlPointsMax) throw new Exception("GPU buffer overflow");
        if (Instance.CurveIngredientsAtoms.Count >= ComputeBufferManager.NumCurveIngredientAtomsMax) throw new Exception("GPU buffer overflow");

        // Upload data
        ComputeBufferManager.Instance.CurveIngredientsAtoms.SetData(CurveIngredientsAtoms.ToArray());
        ComputeBufferManager.Instance.CurveIngredientsAtomCount.SetData(CurveIngredientsAtomCount.ToArray());
        ComputeBufferManager.Instance.CurveIngredientsAtomStart.SetData(CurveIngredientsAtomStart.ToArray());
        ComputeBufferManager.Instance.CurveIngredientsInfos.SetData(CurveIngredientsInfos.ToArray());
        ComputeBufferManager.Instance.CurveIngredientsColors.SetData(CurveIngredientsColors.ToArray());
        ComputeBufferManager.Instance.CurveIngredientsToggleFlags.SetData(CurveIngredientToggleFlags.ToArray());
        ComputeBufferManager.Instance.CurveControlPointsInfos.SetData(CurveControlPointsInfos.ToArray());
        ComputeBufferManager.Instance.CurveControlPointsNormals.SetData(CurveControlPointsNormals.ToArray());
        ComputeBufferManager.Instance.CurveControlPointsPositions.SetData(CurveControlPointsPositions.ToArray());
    }

    public void UploadIngredientToggleData()
    {
        ComputeBufferManager.Instance.ProteinToggleFlags.SetData(ProteinToggleFlags.ToArray());
        ComputeBufferManager.Instance.CurveIngredientsToggleFlags.SetData(CurveIngredientToggleFlags.ToArray());
    }

    #endregion
}
